FROM ruby:2.5.3

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y nodejs mysql-client sqlite3 vim --no-install-recommends && rm -rf /var/lib/apt/lists/*

# Install crond
RUN apt-get update -qq \
    && touch /var/log/cron.log && apt-get -y install cron

# RUN apt-get install libmysqlclient-dev
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/

# RUN bundle update mimemagic
RUN bundle install

COPY . /usr/src/app
# RUN bundle exec rake

EXPOSE 3000
# CMD bash -c "bundle exec whenever --update-crontab && rails server -b 0.0.0.0 -p 3000"

# CMD bash -c "rails db:migrate && rails server -b 0.0.0.0 -p 3000"
CMD bash -c "rails server -b 0.0.0.0 -p 3000"
# CMD ["rails", "server", "-b", "0.0.0.0", "-p", "3000"]