source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# --------------------------- _Whenever for crontab ---------------------------------- #
gem 'whenever', require: false

# -------------------------------------- _Redis -------------------------------------- #
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 3.0'
gem 'redis-namespace'
gem 'redis-rails'
gem 'redis-rack-cache'

# ------------------------------------- Adding rails swagger -------------------------- #
gem 'rswag'
# gem 'rswag-api'
# gem 'rswag-ui'


# gem 'mimemagic', '~> 0.3.4'
gem 'mimemagic', github: 'mimemagicrb/mimemagic', ref: '01f92d86d15d85cfd0f20dabd025dcbd36a8a60f'
gem 'countries'

gem "fog-aws"
gem "sitemap_generator"

gem 'firebase'
gem 'country-select'
gem 'api-pagination'
gem 'httparty', '~> 0.13.7'
gem 'simple_token_authentication', '~> 1.0'
gem 'omniauth-google-oauth2'
# gem 'omniauth', '~> 1.2.2'
# gem 'omniauth-google-oauth2'
gem "koala"
gem "figaro"
gem 'aws-sdk', '~> 3'
gem 'active_model_serializers'
gem 'will_paginate', '~> 3.0.5'
gem 'mysql2'
gem 'rack-cors', :require => 'rack/cors'
gem 'bcrypt', '~> 3.1', '>=3.1.12'
gem 'bcrypt-ruby'
gem 'knock'
gem 'shrine'
gem 'activeadmin'
gem 'json'

# Plus integrations with:
gem 'minitest', '~>5.1'
gem 'builder', '~>3.1'
gem 'rake', '>= 0.8.7'
gem 'thor', '>= 0.18.1'
gem 'method_source', '>= 0'

gem 'pry'
gem 'cancancan'
gem 'draper'
gem 'pundit'

gem 'carrierwave'
gem 'mini_magick'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.3'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'rspec-rails'
  gem 'rspec_api_documentation'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # gem 'seed_dump'
end

group :development do
  gem "faker"
  gem 'rspec-rails', '~> 3.5'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
