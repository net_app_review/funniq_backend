ActiveAdmin.register ArticleTagRecord, :as => "ArticleTagRecord" do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #
  permit_params  :article_id, :tag_id
    
    
  show do
    attributes_table do
      row :article_id do |a|
        a.article
      end
      row :tag_id do |a|
        a.tag
      end
      # row :image_id do |comment|
      #   comment.image
      # end            
    end
  end
    
  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :article_id, :as => :select, :collection => Article.all.ids
      f.input :tag_id, :as => :select, :collection => Tag.all
      # f.input :image_id, :as => :select, :collection => Image.all
    end
    actions
  end
    
end
    