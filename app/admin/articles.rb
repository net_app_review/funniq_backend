ActiveAdmin.register Article do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :header, :uuid, :sub_header, :content,
              :category_id, :sensitive,
              :user_id, :view, :likenumber, :comment_count, :status,
              :points, :image_id, :comment_id, :video_id, :slug, tag_ids: []
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do 

  attributes_table do
    row :id
    row :uuid
    row :header
    row :content
    row :view
    row :comment_count
    row :slug
    row :status
    row :comment_id do |a|
      a.comments
    end

    row :categories_id do |a|
      a.category
    end

    row :user_id do |a|
      a.user
    end

    row :image_id do |a|
      a.image
    end

    row :video_id do |a|
      a.video
    end

    row :sensitive
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :uuid
    f.input :header
    f.input :slug
    f.input :content
    f.input :view
    f.input :user_id, :as => :select, :collection => User.all
    f.input :category_id, :as => :select, :collection => Category.all
    f.input :status, :as => :select, :collection => ["approved","pending","cancelled"]
    f.input :sensitive
    # f.input :tag_ids, as: :select, multiple: true, collection: Tag.all
  end
  actions
end

# action_item  only: :show do
#   link_to 'View on site', send_for_approval_admin_article_path, method: :post
# end

# action_item do
#   link_to "Upload to Facebook", new_article_path
# end



member_action :send_for_approval, method: :post do
  # send your email here you can access to :resource which is your record
  # YourMailer.with(support_session_id: resource.id).your_email.deliver_now
  # redirect to your admin index or show path
  page_id = 1
  
  
  #long live access token
  user_access_token = "EAAQe2fv3lI4BAAs9NLmCKZASjofAGdck6yiYoKQTtsFrFIU7kzmGtsLoQlj1iqKp6Pa2UEZAZCGdrMQu2RZCp3RMJnX8ZBds8QB0TSx4rgfiOE6iAaXHYjucm9ZClsf8JtQETuobznjUVJVZCJuwUAb5saTpwg5jzomFCjKwbgn6gZDZD"

  @graph = Koala::Facebook::API.new(user_access_token)  

  # retrieve collection for all your managed pages: returns collection of hashes with page id, name, category, access token and permissions
  # post = @graph.put_wall_post("hey, i'm learning koala")
  pages = @graph.get_connections('me', 'accounts')  
  # # get access token for first page
  # first_page_token = pages.first['access_token']

  # @page_graph = Koala::Facebook::API.new(first_page_token)
  # @page_graph.put_wall_post('post on page wall')

  # retrieve collection for all your managed pages: returns collection of hashes with page id, name, category, access token and permissions
  # pages = @user_graph.get_connections("me", "feed", :message => "I am writing on my wall!")

  # user = @graph.get_object("me")

  # pages = user.put_wall_post("Hello there!", {
  #   "name" => "Link name",
  #   "link" => "http://www.example.com/",
  #   "caption" => "{*actor*} posted a new review",
  #   "description" => "This is a longer description of the attachment",
  #   "picture" => "https://i.kym-cdn.com/news/posts/original/000/000/421/Screen_Shot_2020-04-24_at_3.40.58_PM.png"
  # })

  # Use that token in another Koala instance if you wish to post as the page
  # page_token = "EAAQe2fv3lI4BAHvxSJZBtjeY3M8vKUDBClujYZBBnvBtkgRRm6HOUZCzKxrgJU6Vhyr2O0NUQiP0vhXGVodfqcGLHEMgJ5aEn5DKBRVq5F2kj2zDvCjHuXNgYwtXHy4CzAymQX6suNrZBaKyyREXUmEJEa4frMwu8NIRC3ywp0LTFuVQGf6C5DreCZAcw5sEZD"

  # page_id = 1
  # message = "hey there"

  # picture_url = 

  # koala_page = Koala::Facebook::API.new(page_token)
  # # koala_page.put_connections(page_id, 'feed', :message => message, :picture => picture_url, :link => link_url)

  # koala_page.put_connections(page_id, 'feed', :message => message, :link => "http://google.com")  
end

end
