ActiveAdmin.register Category do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :image, :icon, :appicon, :description, :image_id, :slug
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
#end
show do
    attributes_table do
      row :id
      row :name
      row :slug
      row :icon      
      row :appicon
      row :description
      row :image_id do |a|
        a.image
      end
  
    end
end
  
form do |f|
  f.inputs "Add/Edit Icon" do
    f.input :name
    f.input :slug
    f.input :icon   
    f.input :appicon
    f.input :description    
  end
  actions
end

end
