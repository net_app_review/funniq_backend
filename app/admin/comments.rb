ActiveAdmin.register Comment, :as => "PostComment" do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params  :content, :article_id, :user_id, :image_id, :points, :subcomment_id, :votefight_id


show do
  attributes_table do
    row :content
    row :points
    row :subcomment_id do |comment|
      comment.subcomments
    end
    row :article_id do |comment|
      comment.article
    end
    row :user_id do |comment|
      comment.user
    end  
    row :votefight_id do |comment|
      comment.votefight
    end  
    # row :image_id do |comment|
    #   comment.image
    # end      
    row :image_id    
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :content
    f.input :points
    f.input :article_id, :as => :select, :collection => Article.all.ids
    f.input :user_id, :as => :select, :collection => User.all
    f.input :votefight_id, :as => :select, :collection => Votefight.all
    # f.input :image_id, :as => :select, :collection => Image.all
  end
  actions
end

end
