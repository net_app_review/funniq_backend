ActiveAdmin.register Competition do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :description, :term, :status, :start, :end
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
  attributes_table do
    row :name
    row :description
    row :term
    row :status
    row :start
    row :end
  end
end

end
