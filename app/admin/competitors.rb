ActiveAdmin.register Competitor do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :paypalaccount, :totalpoints, :ranking, :user_id, :competition_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
  attributes_table do
    row :paypalaccount
    row :totalpoints
    row :ranking
    row :user_id do |competitor|
      competitor.user
    end
    row :competition_id do |competitor|
      competitor.competition
    end
  end
end
  
form do |f|
  f.inputs "Add/Edit Article" do
    f.input :paypalaccount
    f.input :totalpoints
    f.input :ranking
    f.input :user, :as => :select, :collection => User.all.ids
    f.input :competition, :as => :select, :collection => Competition.all.ids
  end
  actions
end

end
