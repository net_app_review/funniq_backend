ActiveAdmin.register Counter do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :countingobj,
                :count,
                :status,
                :countertype

  show do
    attributes_table do
      row :countingobj
      row :count
      row :status
      row :countertype
    end
  end

  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :countingobj
      f.input :count
      f.input :status
      f.input :countertype
    end
    actions
  end

end
