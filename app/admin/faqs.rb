ActiveAdmin.register Faq do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #
    permit_params :question, :question_anchor, :answer
    #
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end
    
    show do
      attributes_table do
        row :question
        row :question_anchor
        row :answer
        # row :userprofile_id do |user|
        #   user.userprofile
        # end      
      end
    end
    
    form do |f|
        f.inputs "Add/Edit FAQ" do
          f.input :question
          f.input :question_anchor
          f.input :answer          
          # f.input :image_id, :as => :select, :collection => Image.all
        #   f.input :category_id, :as => :select, :collection => Category.all
        end
        actions
      end
    
    end
    