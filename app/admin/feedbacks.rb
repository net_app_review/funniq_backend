ActiveAdmin.register Feedback do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :id, :user_id, :username,
                :email, :is_anonymous,
                :content, :rating, :feedback_type
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  #end
  show do
    attributes_table do
      row :id
      row :user_id
      row :username
      row :email
      row :is_anonymous
      row :content
      row :rating
      row :feedback_type
    end
  end

  form do |f|
    f.inputs 'Add/Edit Icon' do
      f.input :user_id
      f.input :username
      f.input :email
      f.input :is_anonymous
      f.input :content
      f.input :rating, as: :select, collection: [1, 2, 3, 4, 5]
      f.input :feedback_type, as: :select, collection: ['feedback', 'bug', 'new feature']
    end
    actions
  end
end
