ActiveAdmin.register FightCandiRec do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #    

    permit_params :vote_candidate_id, :votefight_id
    #
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end
    
    show do
        attributes_table do
            row :vote_candidate_id do |c|
                c.vote_candidate
            end 
            row :votefight_id do |c|
                c.votefight
            end       
        end
    end
    
    form do |f|
        f.inputs "Add/Edit Article" do
          f.input :vote_candidate, :as => :select, :collection => VoteCandidate.ids
          f.input :votefight, :as => :select, :collection => Votefight.all
        end
        actions
    end
        
end
    