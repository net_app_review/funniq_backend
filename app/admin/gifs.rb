ActiveAdmin.register Gif do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :url, :height, :width, :article_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
    attributes_table do
      row :url
      row :height
      row :width
      row :article
      # row :image_id do |comment|
      #   comment.image
      # end      
    end
  end
  
  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :url
      f.input :height
      f.input :width
      f.input :article
    end
    actions
  end
  

end
