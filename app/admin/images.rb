ActiveAdmin.register Image do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params  :name, :url, :height, :width, :article_id, :userprofile_id, :comment_id, :subcomment_id, :category_id

show do
  attributes_table do
    row :name
    row :url
    row :height
    row :width
    row :article_id do |image|
      image.article
    end

    row :userprofile_id do |image|
      image.userprofile
    end

    row :category_id do |image|
      image.category
    end
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :name
    f.input :url
    f.input :height
    f.input :width
    # f.input :article
    f.input :userprofile, :as => :select, :collection => Userprofile.all.ids
    f.input :article, :as => :select, :collection => Article.all.ids
    f.input :category, :as => :select, :collection => Category.all.ids
  end
  actions
end


end
