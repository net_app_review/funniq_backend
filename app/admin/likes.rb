ActiveAdmin.register Like do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #
    # permit_params :list, :of, :attributes, :on, :model
    #
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end
    
    permit_params  :article_id, :user_id,
                   :comment_id, :subcomment_id,
                   :liketype, :feedback_id
    
    show do
      attributes_table do
        # row :name
        # row :url
        # row :height
        # row :width
        # row :article    
        row :liketype
        row :article_id do |like|
          like.article
        end
        row :user_id do |like|
          like.user
        end
        row :comment_id do |like|
          like.comment
        end
        row :subcomment_id do |like|
          like.subcomment
        end
        row :feedback_id do |like|
          like.feedback
        end
      end
    end
    
    form do |f|
      f.inputs "Add/Edit Article" do
        f.input :user, :as => :select, :collection => User.all.ids
        f.input :article, :as => :select, :collection => Article.all.ids
        f.input :comment, :as => :select, :collection => Comment.all.ids
        f.input :subcomment, :as => :select, :collection => Subcomment.all.ids
        f.input :feedback, :as => :select, :collection => Feedback.all.ids
        f.input :liketype, :as => :select, :collection => ["like","dislike","no_value","upvote","hide"]
      end
      actions
    end
    
    
    end
    