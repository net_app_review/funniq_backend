ActiveAdmin.register Newfeature do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  permit_params  :name, :platform, :description, :status, :start, :end

  show do
    attributes_table do
      # row :name
      # row :url
      # row :height
      # row :width
      # row :article    
      row :name
      row :platform
      row :description
      row :status
      row :start
      row :end
    end
  end
    
  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :name
      f.input :platform, :as => :select, :collection => ["web","mobile"]
      f.input :description
      f.input :status, :as => :select, :collection => ["planning","in-progress","finished","cancelled"]
      f.input :start
      f.input :end
    end
    actions
  end
end
    