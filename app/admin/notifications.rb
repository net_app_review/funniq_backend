ActiveAdmin.register Notification do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :content, :article_id, :user_id, :comment_id, :subcomment_id, :creator, :iswatched

  show do
    attributes_table do
      row :content
      row :article_id do |a|
        a.article
      end
      row :user_id do |a|
        a.user
      end
      row :comment_id do |a|
        a.comment
      end
      row :subcomment_id do |a|
        a.subcomment
      end      
      row :creator
      row :iswatched
    end
  end

  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :content
      f.input :creator
      f.input :iswatched
      f.input :user_id, :as => :select, :collection => User.all
      f.input :article_id, :as => :select, :collection => Article.all
      f.input :comment_id, :as => :select, :collection => Comment.all
      f.input :subcomment_id, :as => :select, :collection => Subcomment.all
    end
    actions
  end

end
