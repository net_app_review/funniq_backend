ActiveAdmin.register Reportcategory do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :severity, :description
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
    attributes_table do
      # row :name
      # row :url
      # row :height
      # row :width
      # row :article    
      row :name
      row :severity
      row :description        
    end
  end
  
  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :name
      f.input :severity
      f.input :description      
      # f.input :report_type, :as => :select, :collection => ["violence","nudity","bully"]      
    end
    actions
  end
  


end
