ActiveAdmin.register Report do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :article_id, :user_id, :comment_id, :subcomment_id, :report_type, :reason, :reportcategory_id
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
    attributes_table do
      # row :name
      # row :url
      # row :height
      # row :width
      # row :article    
      row :report_type
      row :reason
      row :article_id do |report|
        report.article
      end
      row :user_id do |report|
        report.user
      end
      row :comment_id do |report|
        report.comment
      end
      row :subcomment_id do |report|
        report.subcomment
      end
      row :reportcategory_id do |report|
        report.reportcategory
      end
    end
  end
  
  form do |f|
    f.inputs "Add/Edit Article" do
      f.input :user, :as => :select, :collection => User.all.ids
      f.input :article, :as => :select, :collection => Article.all.ids
      f.input :comment, :as => :select, :collection => Comment.all.ids
      f.input :subcomment, :as => :select, :collection => Subcomment.all.ids      
      f.input :reportcategory, :as => :select, :collection => Reportcategory.all    
      # f.input :report_type, :as => :select, :collection => ["violence","nudity","bully"]
      f.input :reason
    end
    actions
  end
  

end
