ActiveAdmin.register Subcomment do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params  :content, :user_id, :comment_id, :image_id, :points, :replytouser

show do
  attributes_table do
    row :comment
    row :content
    row :points
    # row :article_id do |comment|
    #   comment.article
    # end
    row :user_id do |sub_comment|
      sub_comment.user
    end  

    row :comment_id do |sub_comment|
      sub_comment.comment
    end  
    row :replytouser
    # row :image_id do |comment|
    #   comment.image
    # end      
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :content
    # f.input :article_id, :as => :select, :collection => Article.all
    f.input :user_id, :as => :select, :collection => User.all
    f.input :comment_id, :as => :select, :collection => Comment.all.ids
    f.input :replytouser
  end
  actions
end


end
