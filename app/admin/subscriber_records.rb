ActiveAdmin.register SubscriberRecord, :as => "SubscriberRecord" do
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  permit_params :subscriber, :subscribeto, :status

  show do
    attributes_table do
      row :subscriber
      row :subscribeto
      row :status
      # row :image_id do |comment|
      #   comment.image
      # end
    end
  end

  form do |f|
    f.inputs "Add/Edit SubscriberRecord" do
      f.input :subscriber
      f.input :subscribeto
      f.input :status, :as => :select, :collection => ["active","inactive"]
    end
    actions
  end
end
