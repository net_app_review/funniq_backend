ActiveAdmin.register Tag do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

    
permit_params  :name, :imageurl, :icon
    
show do
  attributes_table do
    row :name
    row :imageurl
    row :icon
    # row :article_id do |comment|
    #   comment.article
    # end
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :name
    # f.input :article_id, :as => :select, :collection => Article.all
    f.input :imageurl
    f.input :icon
  end
  actions
end



end
