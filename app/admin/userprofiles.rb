ActiveAdmin.register Userprofile do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #
    permit_params :gender, :birthday, :country, :description, :user_id, :image_id
    #
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end

    show do
        attributes_table do
          row :gender
          row :birthday
          row :country
          row :description
          # row :article_id do |comment|
          #   comment.article
          # end
          row :user_id do |userprofile|
            userprofile.user
          end
          row :image_id do |userprofile|
            userprofile.image
          end
        end
      end

    form do |f|
        f.inputs "Add/Edit User" do
          f.input :age
          f.input :gender, :as => :select, :collection => ["male","female","unspecified"]
          f.input :birthday
          f.input :country, as: :select, collection: country_dropdown
          f.input :description
          f.input :user_id, :as => :select, :collection => User.all.ids
        #   f.input :category_id, :as => :select, :collection => Category.all
        end
        actions
      end
    
    end
    