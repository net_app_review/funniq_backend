ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name,
              :uuid,
              :provider,
              :slug,
              :email,
              :password,
              :password_confirmation,
              :userprofile_id,
              :status,
              :level,
              :points,
              :password_init,
              :password_digest,
              :lastloginip
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

index do
  id_column
    column :name
    column :slug
    column :uuid
    column :provider
    column :email
    column :userprofile_id do |user|
      user.userprofile
    end 
    column :status
    column :level
    column :points
    column :lastloginip
  actions
end

show do
  attributes_table do
    row :name
    row :slug
    row :provider
    row :uuid
    row :email
    # row :article_id do |comment|
    #   comment.article
    # end
    row :userprofile_id do |user|
      user.userprofile
    end
    row :status
    row :password_init
    row :password_digest
    row :level
    row :points
  end
end

form do |f|
    f.inputs "Add/Edit User" do
      f.input :name
      f.input :uuid
      f.input :provider
      f.input :slug
      f.input :email
      f.input :status, :as => :select, :collection => ["active","inactive"]
      f.input :password
      f.input :password_confirmation
      f.input :password_init
      f.input :points
    end
    actions
  end

end
