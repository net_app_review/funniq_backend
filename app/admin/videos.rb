ActiveAdmin.register Video do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :url, :article_id, :height, :width, :thumbnail
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

show do
  attributes_table do
    row :name
    row :url
    row :article_id
    row :width
    row :height
    row :thumbnail
  end
end

form do |f|
  f.inputs "Add/Edit Article" do
    f.input :name
    f.input :url
    f.input :thumbnail
    f.input :height
    f.input :width
    f.input :article_id, :as => :select, :collection => Article.all
  end
  actions
end

end
