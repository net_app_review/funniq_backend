ActiveAdmin.register VoteCandidate do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #    

    permit_params :slogan, :color, :name, :image_url, :profile
    #
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end
    
    show do
        attributes_table do
          row :name
          row :image_url
          row :profile
          row :slogan            
          row :color                      
        end
    end
    
    form do |f|
        f.inputs "Add/Edit Article" do
          f.input :name
          f.input :image_url
          f.input :profile
          f.input :color
          f.input :slogan          
        end
        actions
    end
        
end
    