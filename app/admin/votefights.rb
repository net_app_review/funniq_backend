ActiveAdmin.register Votefight do
    # See permitted parameters documentation:
    # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
    #    

    permit_params :name, :date, :criteria, :description, :slogan, :slug, :start_date, :end_date
    # or
    #
    # permit_params do
    #   permitted = [:permitted, :attributes]
    #   permitted << :other if params[:action] == 'create' && current_user.admin?
    #   permitted
    # end
    
    show do
        attributes_table do
            row :name
            row :start_date
            row :end_date                  
            row :description
            row :slogan
            row :slug  
            row :criteria
        end
    end
    
    form do |f|
        f.inputs "Add/Edit Article" do
          f.input :name
          f.input :start_date
          f.input :end_date
          f.input :criteria
          f.input :description
          f.input :slogan
          f.input :slug
        #   f.input :vote_candidates, :as => :select, :collection => VoteCandidate.all
        end
        actions
    end
        
end
    