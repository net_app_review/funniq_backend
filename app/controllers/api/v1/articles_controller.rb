module Api::V1
  class ArticlesController < ApiController
    before_action :set_article, only: [:show, :update]

    PAGINATION_NUMBER = 5
    INDEX_PAGINATION_NUMBER = 2

    def user_articles
      user = User.find_by(uuid: params[:user_id].split('-')[0])

      @articles = Article.where(user_id: user.id)
                         .where(status: 'approved')
                         .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                         .order('created_at DESC')
      render json: @articles, each_serializer: ArticleSerializer
    end

    def category_articles
      @articles = Article.where(category_id: params[:category_id])
                         .where(status: 'approved')
                         .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                         .order('created_at DESC')
      render json: @articles, each_serializer: ArticleSerializer
    end

    def tag_articles
      @articles = Tag.find(params[:tag_id])
                     .articles
                     .where(status: 'approved')
                     .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                     .order('created_at DESC')
      render json: @articles, each_serializer: ArticleSerializer
    end

    # GET /articles
    def index
      if current_user
        @articles = Article.where(status: 'approved')
                           .paginate(page: params[:page], per_page: INDEX_PAGINATION_NUMBER)
                           .order('created_at DESC')#.to_json(:include =>{:user=>{:only => [:id, :name]}, :comments => {:only =>[:id]}})
        if params[:page].to_i < 5
          _get_video_articles.each do |a|
            @articles = [*@articles].unshift(a)
          end
        end
      else
        @articles = Article.where(status: 'approved', sensitive: false)
                           .paginate(page: params[:page], per_page: INDEX_PAGINATION_NUMBER)
                           .order('created_at DESC')

        if params[:page].to_i > 7
          @articles = Article.where(status: 'approved', sensitive: false)
                             .paginate(page: params[:page], per_page: 5)
                             .order('created_at DESC')
        end

        if params[:page].to_i <= 7
          _get_video_articles.each do |a|
            @articles = [*@articles].unshift(a)
          end
        end

        if params[:page].to_i > 1
          @articles = [*@articles].push(_get_sensitive_article)
        end

      end

      render json: @articles, each_serializer: ArticleSerializer
    end

    def _get_sensitive_article
      sensitive_articles = Article.where(sensitive: true)
      sensitive_count = sensitive_articles.count
      sensitive_offset = rand(sensitive_count)
      # sensitive_articles.offset(sensitive_offset).first
      sensitive_articles[sensitive_offset]
    end

    def _get_video_articles
      Article.where(status: 'approved', sensitive: false)
             .joins(:video)
             .where('videos.id IS NOT NULL')
             .order('videos.created_at DESC')
             .limit(50)
             .drop(10)
             .sample(1)
    end

    def trending_articles
      trending = Article.where(status: 'approved')
                        .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                        .order('view DESC')
      render json: trending
    end

    def fresh_articles
      trending = Article.where(status: 'approved')
                        .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                        .order("updated_at DESC")
      render json: trending
    end

    def hot_articles
      hot = Article.where(status: 'approved')
                   .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                   .order("view DESC")
      render json: hot
    end

    # GET /articles/1
    def show
      if @article.status == 'approved'
        if @article.view == nil
          @article.view = 1
        else
          @article.view = @article.view + 1
        end
        @article.user.increment!(:points, 1)
        @article.save
        # render json: @article.to_json(:include =>{:category=>{:only => [:id, :name, :icon]}})
        render json: @article, serializer: SinglearticleSerializer
      end
    end

    def standardizeTags(tagslist)
      tags_array = tagslist.split(',')
      tags_array
    end

    def createTagIfNotExist(tag_name)
      if Tag.exists?(:name => tag_name)
        tag = Tag.find_by_name(tag_name)
        return tag
      else
        tag = Tag.create!(:name => tag_name)
        return tag
      end
    end

    # POST /articles
    def create
      if current_user
        @article = Article.new(article_params)
        @article.user = current_user
        @article.category = Category.find_by(:name => relationship_params[:category])
        @article.view = 0

        if article_params[:sensitive] == true
          @article.category = Category.find_by_name('nsfw')
          if !relationship_params[:tags].include? 'nsfw'
            ArticleTagRecord.create!(article: @article, tag: Tag.find_by_name('nsfw'))
          end
        end

        if @article.save
          if relationship_params[:tags]
            tags_array = standardizeTags(relationship_params[:tags])
            tags_array.each do |t|
              tag = createTagIfNotExist(t)
              ArticleTagRecord.create!(
                article: @article,
                tag: tag
              )
            end
          end

          if relationship_params[:mediatype] == 'image'
            Image.create!(
              width: relationship_params[:width],
              height: relationship_params[:height],
              name: article_params[:name],
              url: relationship_params[:url],
              article: @article)
          elsif relationship_params[:mediatype] == "video"
            Video.create!(
              width: relationship_params[:width],
              height: relationship_params[:height],
              name: article_params[:name],
              url: relationship_params[:url],
              thumbnail: relationship_params[:thumbnail],
              article: @article)
          end
          render json: @article, status: :created
        else
          if @article.errors[:article_limit_attribute]
            render json: {
              messages: @article.errors[:article_limit_attribute],
              is_success: false,
              data:{}
            }, status: :unprocessable_entity
          else
            render json: @article.errors, status: :unprocessable_entity
          end
        end

      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    # def validate_sensitivity(is_sensitive, article)
    #   if is_sensitive
    #     article.category_id = Category.find_by_name('nsfw').id
    #     if !self.tags.where(name: 'nsfw').present?
    #       ArticleTagRecord.create(article: self, tag: Tag.find_by_name('nsfw'))
    #     end
    #   end
    # end

    def checkTagRecordExist(tag_name, article_id)
      exist = false
      if ArticleTagRecord.where(tag_id: Tag.where(name: tag_name).id, article_id: article_id ).present?
        exist = true
      end
      exist
    end

    # PATCH/PUT /articles/1
    def update
      if current_user then
        if current_user.id == @article.user.id
          if (article_params[:sensitive] == true)
            @article.category = Category.find_by_name('nsfw')
            # if !relationship_params[:tags].include? 'nsfw'
            #   ArticleTagRecord.create!(article: @article, tag: Tag.find_by_name('nsfw'))
            # end
          else
            @article.category = Category.find_by_name(relationship_params[:category])
          end

          if @article.update(article_params)
            if relationship_params[:tags]
              tags_array = standardizeTags(relationship_params[:tags])
              ArticleTagRecord.where(article_id: @article.id).destroy_all
              tags_array.each do |t|
                tag = createTagIfNotExist(t)
                ArticleTagRecord.create!(
                  article: @article,
                  tag: tag
                )
              end
              if (article_params[:sensitive] == true)
                if !relationship_params[:tags].include? 'nsfw'
                  ArticleTagRecord.create!(article: @article, tag: Tag.find_by_name('nsfw'))
                end
              end
            end

            render json: @article
          else
            render json: @article.errors, status: :unprocessable_entity
          end
        else
          render json: {
            messages: "Unauthorized",
            is_success: false,
            data:{}
          }, status: :unauthorized 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end

    # DELETE /articles/1
    def destroy
      if current_user
        if Article.find_by(id: params[:id])
          @article = Article.find_by(id: params[:id])
          if @article.user.id == current_user.id
            if @article.update!(status: "deleted")
              render json: {
                messages: "Article is deleted",
                is_success: true,
                data:{}
              }, status: :ok
            else
              render json: {
                messages: "cant delete article, something is wrong",
                is_success: false,
                data:{}
              }, status: :conflict
            end
          else
            render json: {
              messages: "Unauthorized",
              is_success: false,
              data:{}
            }, status: :unauthorized
          end
        else
          render json: {
            messages: "cant-find-article",
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find_by(uuid: params[:id].split('-')[0])
      # @article = Article.find(params[:id])
    end

    def relationship_params
      params.require(:article).permit(:category,
                                      :mediatype, :url,
                                      :width, :height, :tags, :thumbnail)
    end

    # Only allow a trusted parameter "white list" through.
    def article_params
      params.require(:article).permit(:name, :header,
                                      :sub_header, :content,
                                      :id, :points, :status,
                                      :sensitive)
    end
  end
end