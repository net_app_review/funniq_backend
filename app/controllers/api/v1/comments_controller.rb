module Api::V1
  class CommentsController < ApiController
    before_action :set_comment, only: [:show, :update]    

    PAGINATION_NUMBER = 10

    def article_comments

      my_comments = []

      article = Article.find_by(uuid: params[:article_id].split('-')[0])

      @comments = Comment.where(article_id: article.id)
                         .paginate(page: params[:page], per_page: 20)
                         .order('created_at DESC')

      if current_user
        my_comments = @comments.where(user_id: current_user.id).limit(1)
      end

      @total_comment = Comment.where(article_id: params[:article_id])

      @total_page = @total_comment.count / PAGINATION_NUMBER

      render json: {
        comments: ActiveModelSerializers::SerializableResource.new(@comments, each_serializer: CommentSerializer, current_user: current_user),
        my_comments: ActiveModelSerializers::SerializableResource.new(my_comments, each_serializer: CommentSerializer, current_user: current_user),
        status: 200
      }

      # render json: @comments

      response.headers['Access-Control-Expose-Headers'] = 'TOTAL_COMMENT,TOTAL_PAGE'

      response.headers['TOTAL_COMMENT'] = @total_comment.count

      response.headers['TOTAL_PAGE'] = @total_page + 1
    end

    def votefight_comments
      
      @comments = Comment.where(votefight_id: params[:votefight_id]).paginate(page: params[:page], per_page: 10).order('created_at DESC')

      @total_comment = Comment.where(votefight_id: params[:votefight_id])

      @total_page = @total_comment.count / PAGINATION_NUMBER

      render json: @comments
      
      response.headers['Access-Control-Expose-Headers'] = 'TOTAL_COMMENT,TOTAL_PAGE'

      response.headers['TOTAL_COMMENT'] = @total_comment.count

      response.headers['TOTAL_PAGE'] = @total_page + 1
    end

    # GET /comments
    # GET /comments.json
    def index
      @comments = Comment.all
    end

    # GET /comments/1
    # GET /comments/1.json
    def show
    end

    # POST /comments
    # POST /comments.json
    def create
      if current_user
        @comment = Comment.new(comment_params)
        @comment.points = 0
        @comment.user = current_user
        @comment.article = Article.find_by(uuid: comment_params[:article_id].split('-')[0])
        # @comment.article = Article.find(comment_params[:article_id])

        if @comment.save
          if image_params[:image_url]
            Image.create!(
              width: image_params[:width],
              height: image_params[:height],
              name: "comment", 
              url: image_params[:image_url],
              comment: @comment
            )
          end
          render json: @comment, status: :created
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized      
      end
    end

    # PATCH/PUT /comments/1
    # PATCH/PUT /comments/1.json
    def update
      if current_user then
        if @comment.update(comment_params)
          render json: @comment, status: :ok
        else
          render json: @comment.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized           
      end
    end

    # DELETE /comments/1
    # DELETE /comments/1.json
    def destroy
      if current_user then      
        if Comment.find_by(id: params[:id]) then
          @comment = Comment.find_by(id: params[:id])
          if @comment.user.id == current_user.id then                                    
            if @comment.destroy
              render json: {
                messages: "Comment is deleted",
                is_success: true,
                data:{}
              }, status: :ok
            else
              render json: {
                messages: "cant delete comment, something is wrong",
                is_success: false,
                data:{}
              }, status: :conflict
            end
          else
            render json: {
              messages: "Unauthorized",
              is_success: false,
              data:{}
            }, status: :unauthorized
          end
        else
          render json: {
            messages: "comment-not-found",
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      params.require(:comment).permit( :image_url, :width, :height)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit( :content, :article_id, :points, :votefight_id)
    end
  end
end