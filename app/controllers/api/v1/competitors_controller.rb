module Api::V1
  class CompetitorsController < ApiController
    before_action :set_competitor, only: [:show, :update, :destroy]

    # GET /competitors
    # GET /competitors.json
    def index
      @competitors = Competitor.all
    end

    # GET /competitors/1
    # GET /competitors/1.json
    def show
    end

    # POST /competitors
    # POST /competitors.json
    def create
      if current_user then
        @competitor = Competitor.new(competitor_params)

        if @competitor.save
          render :show, status: :created, location: @competitor
        else
          render json: @competitor.errors, status: :unprocessable_entity
        end
      end
    end

    # PATCH/PUT /competitors/1
    # PATCH/PUT /competitors/1.json
    def update
      if @competitor.update(competitor_params)
        render :show, status: :ok, location: @competitor
      else
        render json: @competitor.errors, status: :unprocessable_entity
      end
    end

    # DELETE /competitors/1
    # DELETE /competitors/1.json
    def destroy
      @competitor.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_competitor
        @competitor = Competitor.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def competitor_params
        params.require(:competitor).permit(:user_id, :competition_id, :paypalaccount, :totalpoints, :ranking)
      end
  end
end