module Api::V1
  class FaqsController < ApiController
    before_action :set_faq, only: [:show, :update, :destroy]

    # GET /faqs
    # GET /faqs.json
    def index
      @faqs = Faq.all
      render json: @faqs
    end

    # GET /faqs/1
    # GET /faqs/1.json
    def show
    end

    # POST /faqs
    # POST /faqs.json
    def create
      @faq = Faq.new(faq_params)

      if @faq.save
        render :show, status: :created, location: @faq
      else
        render json: @faq.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /faqs/1
    # PATCH/PUT /faqs/1.json
    def update
      if @faq.update(faq_params)
        render :show, status: :ok, location: @faq
      else
        render json: @faq.errors, status: :unprocessable_entity
      end
    end

    # DELETE /faqs/1
    # DELETE /faqs/1.json
    def destroy
      @faq.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_faq
        @faq = Faq.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def faq_params
        params.require(:faq).permit(:question, :question_anchor, :answer)
      end
  end
end