module Api::V1
  class FeedbacksController < ApiController
    before_action :set_feedback, only: [:show, :update]

    PAGINATION_NUMBER = 5

    # GET /feedbacks
    def index
      @feedbacks = Feedback.where(feedback_type: convert_feedback_type(params[:feedback_type].to_i))
                           .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                           .order('created_at DESC')
      render json: @feedbacks, each_serializer: FeedbackSerializer
    end

    def convert_feedback_type(feedback_int)
      feedback_type = ''
      if feedback_int == 1
        feedback_type = 'feedback'
      elsif feedback_int == 2
        feedback_type = 'bug'
      elsif feedback_int == 3
        feedback_type = 'new feature'
      end
      feedback_type
    end
    
    # GET /feedbacks/1
    def show

      if @feedback.view == nil
        @feedback.view = 1
      else
        @feedback.view = @feedback.view + 1
      end
      @feedback.user.increment!(:points, 1)
      @feedback.save
      # render json: @feedback.to_json(:include =>{:category=>{:only => [:id, :name, :icon]}})
      render json: @feedback, serializer: SinglefeedbackSerializer
    end

    # POST /feedbacks
    def create
      @feedback = Feedback.new(feedback_params)
      @feedback.feedback_type = convert_feedback_type(feedback_params[:feedback_type].to_i)
      if current_user
        @feedback.user_id = current_user.id
        @feedback.username = current_user.name
        @feedback.email = current_user.email        
        if @feedback.save
          render json: @feedback, status: :created
        else
          render json: @feedback.errors, status: :unprocessable_entity
        end
      else
        if @feedback.save
          render json: @feedback, status: :created
        else
          render json: @feedback.errors, status: :unprocessable_entity
        end
      end
    end


    # PATCH/PUT /feedbacks/1
    def update
      if current_user then
        if current_user.id == @feedback.user.id
          if (feedback_params[:sensitive] == true)
            @feedback.category = Category.find_by_name('nsfw')
            # if !relationship_params[:tags].include? 'nsfw'
            #   ArticleTagRecord.create!(feedback: @feedback, tag: Tag.find_by_name('nsfw'))
            # end
          else
            @feedback.category = Category.find_by_name(relationship_params[:category])
          end

          if @feedback.update(feedback_params)
            if relationship_params[:tags]
              tags_array = standardizeTags(relationship_params[:tags])
              ArticleTagRecord.where(feedback_id: @feedback.id).destroy_all
              tags_array.each do |t|
                tag = createTagIfNotExist(t)
                ArticleTagRecord.create!(
                  feedback: @feedback,
                  tag: tag
                )
              end
              if (feedback_params[:sensitive] == true)
                if !relationship_params[:tags].include? 'nsfw'
                  ArticleTagRecord.create!(feedback: @feedback, tag: Tag.find_by_name('nsfw'))
                end
              end
            end

            render json: @feedback
          else
            render json: @feedback.errors, status: :unprocessable_entity
          end
        else
          render json: {
            messages: "Unauthorized",
            is_success: false,
            data:{}
          }, status: :unauthorized 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end

    # DELETE /feedbacks/1
    def destroy
      if current_user then
        if Feedback.find_by(id: params[:id]) then
          @feedback = Feedback.find_by(id: params[:id])
          if @feedback.user.id == current_user.id then
            if @feedback.destroy
              render json: {
                messages: "Feedback is deleted",
                is_success: true,
                data:{}
              }, status: :ok
            else
              render json: {
                messages: "cant delete feedback, something is wrong",
                is_success: false,
                data:{}
              }, status: :conflict
            end
          else
            render json: {
              messages: "Unauthorized",
              is_success: false,
              data:{}
            }, status: :unauthorized
          end
        else
          render json: {
            messages: "cant-find-feedback",
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data: {}
        }, status: :unauthorized
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_feedback
      @feedback = Feedback.find(params[:id])
    end
    
    # Only allow a trusted parameter "white list" through.
    def feedback_params
      params.require(:feedback).permit(:user_id, :username, :email,
                                       :is_anonymous, :content,
                                       :rating, :feedback_type)
    end
  end
end