require 'aws-sdk-s3'
module Api::V1
  class ImagesController < ApiController
    before_action :set_image, only: [:show, :update]
    before_action :set_s3_credentials, only: [:request_for_presigned_url]

    def request_for_presigned_url
      filename = "podioissue.png"
      #   aws_credentials = Aws::Credentials.new(
      #     ENV['AWS_ACCESS_KEY_ID'],
      #     ENV['AWS_SECRET_ACCESS_KEY']
      #   )
        
      #   @s3_bucket = Aws::S3::Resource.new(
      #     region: 'us-east-1',
      #     credentials: aws_credentials
      #   ).bucket('funnyappimagestorage')
      #   # ).bucket(ENV['S3_BUCKET'])              

      # presigned_url = @s3_bucket.presigned_post(
      #   key: "#{Rails.env}/#{SecureRandom.uuid}/${filename}",
      #   success_action_status: '201',
      #   signature_expiration: (Time.now.utc + 15.minutes)
      # )                    
            
      # signer = Aws::S3::Presigner.new
      # url = signer.presigned_url(:put_object,
      #                             bucket: 'funnyappimagestorage',
      #                             key: filename,
      #                             acl: 'public-read',
      #                             content_type: "image/#{image_type}")
            

      s3 = Aws::S3::Client.new(
        region:               'ap-southeast-1', #or any other region
        access_key_id:        ENV['AWS_ACCESS_KEY_ID'],
        secret_access_key:    ENV['AWS_SECRET_ACCESS_KEY']
      )

      key = "#{filename}-#{SecureRandom.uuid}"

      signer = Aws::S3::Presigner.new(client: s3)
      url = signer.presigned_url(
        :put_object,
        bucket: 'funnyappimagestorage',
        key: key
      )      
      response = { presigned_url: url , image_url: "https://funnyappimagestorage.s3-ap-southeast-1.amazonaws.com/" + key}      

      # data = { url: presigned_url.url, url_fields: presigned_url.fields }
    
      render json: response
    end

    # GET /images
    # GET /images.json
    def index
      @images = Image.all
    end

    # GET /images/1
    # GET /images/1.json
    def show
    end

    # POST /images
    # POST /images.json
    def create
      @image = Image.new(image_params)

      if @image.save
        render :show, status: :created, location: @image
      else
        render json: @image.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /images/1
    # PATCH/PUT /images/1.json
    def update
      if current_user then 
        if @image.update(image_params)
          render json: {
            messages: "Image is succesfully updated",
            is_success: true,
            data:{
              image: @image
            }
          }, status: :ok                   
        else
          render json: @image.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized           
      end
    end
    
    # DELETE /images/1
    # DELETE /images/1.json
    def destroy

      @firebase = Firebase::Client.new(ENV["FIREBASE_DATABASE_URL"], ENV["FIREBASE_DATABASE_SECRET"])
      if current_user then      
        if Image.find_by(id: params[:id]) then
          @image = Image.find_by(id: params[:id])          
            if @firebase.delete(@image.url).response.status_code == 204 then              
              @image.destroy    
              render json: {
                messages: "image is destroyed",
                is_success: true,
                data:{}
              }, status: :ok                      
            elsif @firebase.delete(@image.url).response.status_code == 404 then
              render json: {
                messages: "cant delete image on firebase",
                is_success: false,
                data:{}
              }, status: :not_found    
            elsif @firebase.delete(@image.url).response.status_code == 401
              render json: {
                messages: "could not parse auth token",
                is_success: false,
                data:{}
              }, status: :not_found    
              else
              render json: {
                messages: "invalid response from firebase",
                is_success: false,
                data:{}
              }, status: :not_found    
            end          
        else
          render json: {
            messages: "image not found",
            is_success: false,
            data:{}
          }, status: :not_found
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end


    end

    private
      def set_s3_credentials

      end

      # Use callbacks to share common setup or constraints between actions.
      def set_image
        @image = Image.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def image_params
        params.require(:image).permit(:name, :url, :article_id, :user_id, 
                                      :comment_id, :subcomment_id, :category_id, 
                                      :tag_id, :category, :height, :width)
      end
  end
end