module Api::V1  
  class LikesController < ApiController
    # before_action :set_like, only: [:show, :update, :destroy]

    # GET /likes
    # GET /likes.json
    def index
      @likes = Like.all
    end

    # GET /likes/1
    # GET /likes/1.json
    def show
    end

    # POST /likes
    # POST /likes.json
    def create
      if current_user
        @like = Like.new(like_params)

        if @like.save
          render :show, status: :created, location: @like
        else
          render json: @like.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized      
      end
    end

    # PATCH/PUT /likes/1
    # PATCH/PUT /likes/1.json
    def update
      if @like.update(like_params)
        render :show, status: :ok, location: @like
      else
        render json: @like.errors, status: :unprocessable_entity
      end
    end
    
    #handling Like actions via API
    def handle_like_feature
      if current_user
        if params[:id]
          if Like.find_by_id(params[:id])
            @like = Like.find(params[:id])
            if @like.update(like_params)
              render json: @like, status: :ok
            else
              render json: @like.errors, status: :unprocessable_entity
            end
          else
            @like = Like.new(like_params)
            @like.user = current_user
            if @like.save
              render json: @like, status: :ok
            else
              render json: @like.errors, status: :unprocessable_entity
            end
          end
        else
          render json: {
            message: "missing like id params",
            data: {}
          }, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized      
      end
    end

    #handle votes    
    # DELETE /likes/1
    # DELETE /likes/1.json
    def destroy
      @like.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_like
        @like = Like.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def like_params
        params.require(:like).permit(:id, :article_id, :comment_id, :subcomment_id, :liketype, :feedback_id)
      end
  end
end