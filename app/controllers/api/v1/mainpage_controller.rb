module Api::V1

    class MainpagesController < ApiController
      before_action :set_mainpage, only: [:show, :update, :destroy]
  
      # GET /mainpages
      def index
        @mainpages = mainpage.all
        @category = Category.all
        haha = {:mainpages => @mainpages, :c => @category}
        render json: haha
      end
  
      # GET /mainpages/1
      def show
  
        if @mainpage.view == nil
          @mainpage.view =1
        else 
          @mainpage.view = @mainpage.view + 1
        end
        @mainpage.save
        render json: @mainpage      
      end
  
      # POST /mainpages
      def create
        @mainpage = mainpage.new(mainpage_params)
  
        if @mainpage.save
          render json: @mainpage, status: :created, location: @mainpage
        else
          render json: @mainpage.errors, status: :unprocessable_entity
        end
      end
  
      # PATCH/PUT /mainpages/1
      def update
        if @mainpage.update(mainpage_params)
          render json: @mainpage
        else
          render json: @mainpage.errors, status: :unprocessable_entity
        end
      end
  
      # DELETE /mainpages/1
      def destroy
        @mainpage.destroy
      end
  
      private
        # Use callbacks to share common setup or constraints between actions.
        def set_mainpage
          @mainpage = mainpage.find(params[:id])
        end
  
        # Only allow a trusted parameter "white list" through.
        def mainpage_params
          params.require(:mainpage).permit(:name, :header, :sub_header, :content, :image)
        end
    end
  end