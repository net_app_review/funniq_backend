module Api::V1
  class NewfeaturesController < ApiController
      before_action :set_newfeature, only: [:show, :update, :destroy]

      # GET /newfeatures
      # GET /newfeatures.json
      def index
        @newfeatures = Newfeature.all
        render json: @newfeature
      end

      # GET /newfeatures/1
      # GET /newfeatures/1.json
      def show
      end

      # POST /newfeatures
      # POST /newfeatures.json
      def create
        @newfeature = Newfeature.new(newfeature_params)

        if @newfeature.save
          render :show, status: :created, location: @newfeature
        else
          render json: @newfeature.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /newfeatures/1
      # PATCH/PUT /newfeatures/1.json
      def update
        if @newfeature.update(newfeature_params)
          render :show, status: :ok, location: @newfeature
        else
          render json: @newfeature.errors, status: :unprocessable_entity
        end
      end

      # DELETE /newfeatures/1
      # DELETE /newfeatures/1.json
      def destroy
        @newfeature.destroy
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_newfeature
          @newfeature = Newfeature.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def newfeature_params
          params.require(:newfeature).permit(:name, :platform, :description, :status, :start, :end)
        end
    end
end