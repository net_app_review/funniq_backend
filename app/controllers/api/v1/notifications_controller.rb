module Api::V1
  class NotificationsController < ApiController
    before_action :set_notification, only: [:show, :update, :destroy]

    # GET /notifications
    # GET /notifications.json
    PAGINATION_NUMBER = 4
    def index
      if current_user
        if params[:iswatched]
          if params[:iswatched] === 'true'
            @notifications = Notification.where(user_id: current_user.id, iswatched: true)
                                        .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                                        .order('created_at DESC')
            # render json: { data: @notifications, status: :ok}
            total_unseen = -1
          elsif params[:iswatched] === 'false'
            @notifications = Notification.where(user_id: current_user.id, iswatched: false)
                                        .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                                        .order('created_at DESC')
            total_unseen = Notification.where(user_id: current_user.id, iswatched: false).count
          end
        else
          @notifications = Notification.where(user_id: current_user.id)
                                       .paginate(page: params[:page], per_page: PAGINATION_NUMBER)
                                       .order('created_at DESC')
          total_unseen = Notification.where(user_id: current_user.id, iswatched: false).count                                    
        end
        render json: { data: ActiveModelSerializers::SerializableResource.new(@notifications, each_serializer: NotificationSerializer), status: 200, total_unseen: total_unseen }
      else
        render json: { data: {}, status: 401, message: 'Unauthenticated' }
      end
    end

    # GET /notifications/1
    # GET /notifications/1.json
    def show
    end

    # POST /notifications
    # POST /notifications.json
    def create
      @notification = Notification.new(notification_params)
      if @notification.save
        # ActionCable.server.broadcast 'notification_channel', { data: @notification, status: :ok}
        render json: @notification
      else
        render json: @notification.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /notifications/1
    # PATCH/PUT /notifications/1.json
    def update



      if @notification.update(notification_params)
        render json: @notification, status: 200        
      else
        render json: @notification.errors, status: :unprocessable_entity
      end
    end

    # DELETE /notifications/1
    # DELETE /notifications/1.json
    def destroy
      @notification.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_notification
        @notification = Notification.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def notification_params
        params.require(:notification).permit(:article_id, :content, :user_id, :comment_id, :creator, :subcomment_id, :iswatched)
      end
  end
end