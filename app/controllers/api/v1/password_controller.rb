
module Api::V1
    class PasswordController < ApiController        
        # before_action :authenticate_with_token, only: [:index, :current, :update, :logout]
        before_action :set_user, only: [:show]
        def forgot
            
            if params[:email].blank? # check if email is present
              return render json: {
                  messages: 'Email not present'
                }, status: :not_found
            else
                user = User.find_by(email: params[:email]) # if present find user by email
        
                if user.present?
                    user.generate_password_token! #generate pass token
                    # SEND EMAIL HERE
                    UserMailer.forgot_password(user).deliver_now
                    render json: {messages: 'password sent to email'}, status: :ok
                else
                    render json: {messages: ['Email address not found. Please check and try again.']}, status: :not_found
                end
            end                
          end
        
          def reset
            token = params[:token].to_s
        
            if params[:token].blank?
                return render json: {error: 'Token not present'}
            else        
                user = User.find_by(reset_password_token: token)
            
                if user.present? && user.password_token_valid?
                    if user.reset_password!(params[:password])
                        render json: {messages: 'password has been resetted'}, status: :ok
                    else
                        render json: {error: user.errors.full_messages}, status: :unprocessable_entity
                    end
                else
                    render json: {error:  ['Link not valid or expired. Try generating a new link.']}, status: :not_found
                end
            end
          end
    end
end