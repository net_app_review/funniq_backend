module Api::V1
    class RegistrationsController < Devise::RegistrationsController
        # Devise::RegistrationsController
        before_action :ensure_params_exist, only: :create
        #sign_up
        def create
                            
            user = User.new(user_params)

            user.provider = "email"
            user.status = "active"
            user.password_init= true
            user.generate_new_authentication_token
            userprofile = Userprofile.new()            
            if user.save
                userprofile.user = user
                userprofile.save
                avatar = Image.new(height: 50,
                    width: 50,
                    userprofile: userprofile,
                    url: "https://ui-avatars.com/api/?name=#{user.slug}")                                    
                avatar.save
                render json: {
                    messages: "Signed Up Successfully",
                    is_success: true,
                    data:{
                        user: AuthenuserSerializer.new(user).attributes,                
                    }
                }, status: :ok
            else
                render json: {
                    messages: "Something wrong",
                    is_success: false,
                    data: {}
                }, status: :unprocessable_entity
            end
        end

        private
        def user_params
            params.require(:user).permit(:email, :password, :password_confirmation,:name)
        end

        def ensure_params_exist
            return if params[:user].present?
            render json: {
                mesaage: "Missing params",
                is_success: false,
                data: {}
            }, status: :bad_request
        end
        # def create        
        
        # user = User.create!(
        #     name: params["user"]["name"],
        #     email: params["user"]["email"],
        #     password: params["user"]["password"],
        #     password_confirmation: params["user"]["password_confirmation"]
        # )
        

        # if user
        #     session[:user_id] = user.id
        #     render json: {
        #         status: :created,
        #         user: user
        #     }
        # else
        #     render json: { status:500 }
        # end
        # end
    end
end