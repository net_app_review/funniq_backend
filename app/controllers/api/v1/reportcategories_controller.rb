module Api::V1
  class ReportcategoriesController < ApiController
    before_action :set_reportcategory, only: [:show, :update, :destroy]

    # GET /reportcategories
    # GET /reportcategories.json
    def index
      # render json: @reportcategory
      @reportcategories = Reportcategory.all
      render json: @reportcategories
    end

    # GET /reportcategories/1
    # GET /reportcategories/1.json
    def show
    end

    # POST /reportcategories
    # POST /reportcategories.json
    def create
      @reportcategory = Reportcategory.new(reportcategory_params)

      if @reportcategory.save
        render :show, status: :created, location: @reportcategory
      else
        render json: @reportcategory.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /reportcategories/1
    # PATCH/PUT /reportcategories/1.json
    def update
      if @reportcategory.update(reportcategory_params)
        render :show, status: :ok, location: @reportcategory
      else
        render json: @reportcategory.errors, status: :unprocessable_entity
      end
    end

    # DELETE /reportcategories/1
    # DELETE /reportcategories/1.json
    def destroy
      @reportcategory.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_reportcategory
        @reportcategory = Reportcategory.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def reportcategory_params
        params.require(:reportcategory).permit(:category)
      end
  end
end