module Api::V1
  class ReportsController < ApiController
      before_action :set_report, only: [:show, :update, :destroy]

      # GET /reports
      # GET /reports.json
      def index
        @reports = Report.all
      end

      # GET /reports/1
      # GET /reports/1.json
      def show
      end

      # POST /reports
      # POST /reports.json
      def create
        if current_user          
          @report = Report.new(report_params)
          @report.user = current_user
          
          if @report.save
            
            if ( report_params[:article_id])              
              article = Article.find_by_id(report_params[:article_id])              
              # if article.reports.count >= 2
              #   article.update(:status => "pending")

              #   ReportedarticleMailer.post_deactive(article.user, article).deliver_now
              # end
            end

            render json: {
                messages: "report created",
                is_success: true,
                data:{
                  report: @report
                }                
              }, status: :ok
          else
            render json: @report.errors, status: :unprocessable_entity
          end
        else
          render json: {
            messages: "Unauthenticated",
            is_success: false,
            data:{}
          }, status: :unauthorized
        end
      end

      # PATCH/PUT /reports/1
      # PATCH/PUT /reports/1.json
      def update
        if @report.update(report_params)
          render :show, status: :ok, location: @report
        else
          render json: @report.errors, status: :unprocessable_entity
        end
      end

      

      # DELETE /reports/1
      # DELETE /reports/1.json
      def destroy
        @report.destroy
      end

      private
        def inactive_reported_articles(article)
        
            
         
        end

        # Use callbacks to share common setup or constraints between actions.
        def set_report
          @report = Report.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def report_params
          params.require(:report).permit(:article_id, :comment_id, :subcomment_id, :report_type, :reason, :reportcategory_id)
        end
    end
end  