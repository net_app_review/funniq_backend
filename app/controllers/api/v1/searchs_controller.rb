module Api::V1
  class SearchsController < ApiController
    PAGINATION_NUMBER = 5

    def search_article
      if !search_params[:search_string].eql? ''
        begin
          @articles = Article.where('header like ?', "%#{search_params[:search_string]}%")
                             .where(status: 'approved')
                             .paginate(page: search_params[:page], per_page: PAGINATION_NUMBER)
                             .order('created_at DESC')

          if @articles.present?        
            render json: @articles, each_serializer: ArticleSerializer
          else
            render json: {
              messages: 'articles not found',
              is_success: false,
              data: {}, status: :ok
            }, status: :not_found
          end
        rescue SystemCallError
          render json: {
            messages: 'internal system error',
            is_success: false,
            data: {}
          }, status: :not_found
        end
      else
        render json: {
          messages: 'missing search string',
          is_success: false,
          data: {}
        }, status: :not_found
      end
    end

    def search_tag
      if !search_params[:search_string].eql? ''
        begin
          @tags = Tag.where('name like ?', "%#{search_params[:search_string]}%")
                    .paginate(page: search_params[:page], per_page: PAGINATION_NUMBER)
                    .order('created_at DESC')

          if @tags.present?
            render json: {
              tags: @tags,
              status: 200
            }
          else
            render json: {
              messages: 'tags not found',
              is_success: false,
              data: {}, status: :ok
            }, status: :not_found
          end
        rescue SystemCallError
          render json: {
            messages: 'internal system error',
            is_success: false,
            data: {}
          }, status: :not_found
        end
      else
        render json: {
          messages: 'missing search string',
          is_success: false,
          data: {}
        }, status: :not_found
      end
    end

    def search_user
      if !search_params[:search_string].eql? ''
        begin
          @users = User.where('name like ?', "%#{search_params[:search_string]}%")
                       .where(status: 'active')
                       .paginate(page: search_params[:page], per_page: PAGINATION_NUMBER)
                       .order('created_at DESC')

          if @users.present?
            render json: @users, each_serializer: PublicUserSerializer
          else
            render json: {
              messages: 'users not found',
              is_success: false,
              data: {}, status: :ok
            }, status: :not_found
          end
        rescue SystemCallError
          render json: {
            messages: 'internal system error',
            is_success: false,
            data: {}
          }, status: :not_found
        end
      else
        render json: {
          messages: 'missing search string',
          is_success: false,
          data: {}
        }, status: :not_found
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.

    # Never trust parameters from the scary internet,
    # only allow the white list through.
    def search_params
      params.require(:search).permit(:search_string, :page)
    end
  end
end
