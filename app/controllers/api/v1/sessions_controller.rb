module Api::V1
    class SessionsController < Devise::SessionsController
        before_action :sign_in_params, only: :create
        before_action :load_user, only: :create
        before_action :valid_token, only: :destroy
        skip_before_action :verify_signed_out_user, only: :destroy
        prepend_before_action :require_no_authentication, only: [:cancel ]

        def create                                  
            if (@user.try(:authenticate, sign_in_params[:password]) && @user.status == "active")                
                                                
                @user.generate_new_authentication_token
                render json: {
                    messages: "Signed In Successfully",
                    is_success: true,
                    data:{
                        user: AuthenuserSerializer.new(@user).attributes                                
                    }
                }
            elsif (@user.try(:authenticate, sign_in_params[:password]) && @user.status == "inactive")                
                render json: {
                    messages: "User is inactive",
                    is_success: false,
                    data: {                        
                        user_status:@user.status
                    }
                }, status: :forbidden
            else
                render json: {
                    messages: "Something wrong",
                    is_success: false,
                    data: {                        
                        user_status:@user.status
                    }
                }, status: :unprocessable_entity
            end
        end

        #log out
        def destroy                   
            # sign_out @user            
            @user.generate_new_authentication_token
            render json: {
                messages: "logged out Successfully",
                is_success: true,
                data:{}
            }
        end

        private

        def sign_in_params
            params.require(:sign_in).permit(:email, :password)
        end

        def load_user 
            @user = User.find_by_email(sign_in_params[:email])
            if @user
                return @user
            else 
                json_response "Cannot get User", false, {}, :failure
            end
        end

        def valid_token
            @user = User.find_by authentication_token: request.headers["X-User-Token"]
            if @user
                return @user
            else
                render json: {
                    messages: "Invalid Token",
                    is_success: false,
                    data: {}
                }, status: :failure
                # json_response , false
            end
        end
        # include CurrentUserConcern
        # def create 
        #     user = User
        #         .find_by(email: params["user"]["email"]).try(:authenticate, params["user"]["password"])
        #         .try(:authenticate, params["user"]["password"])            
        #     if user
        #         session[:user_id] = user.id
        #         render json:{
        #             status: :created,
        #             logged_in: true,
        #             user: user
        #         }
        #     else
        #         render json: { status: 401 }
        #     end                
        # end

        # def logged_in    
        #     if @current_user
        #         render json:{
        #             logged_in: true,
        #             user: @current_user
        #         }
        #     else
        #         render json:{
        #             logged_in: false
        #         }
        #     end
        # end

        # def logout
        #     reset_session
        #     render json: { status: 200, logged_out: true}
        # end
    end 
end