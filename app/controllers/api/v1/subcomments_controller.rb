module Api::V1 
  class SubcommentsController < ApiController
    # before_action :set_subcomment, only: [:show, :update, :destroy]

    def comment_subcomments
      @subcomments = Subcomment.where(comment_id: params[:comment_id]).paginate(page: params[:page], per_page: 10)
      render json: @subcomments
    end

    # GET /subcomments
    # GET /subcomments.json
    def index
      @subcomments = Subcomment.all
    end

    # GET /subcomments/1
    # GET /subcomments/1.json
    def show
    end

    # POST /subcomments
    # POST /subcomments.json
    def create
      if current_user
        @subcomment = Subcomment.new(subcomment_params)
        @subcomment.user = current_user
        @subcomment.points = 0        
        
        if @subcomment.save
          render json: @subcomment, status: :created
        else
          render json: @subcomment.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end

    # PATCH/PUT /subcomments/1
    # PATCH/PUT /subcomments/1.json
    def update
      if @subcomment.update(subcomment_params)
        render :show, status: :ok, location: @subcomment
      else
        render json: @subcomment.errors, status: :unprocessable_entity
      end
    end

    # DELETE /subcomments/1
    # DELETE /subcomments/1.json
    def destroy
      if current_user then      
        if Subcomment.find_by(id: params[:id]) then
          @subcomment = Subcomment.find_by(id: params[:id])
          if @subcomment.user.id == current_user.id then                                    
            if @subcomment.destroy
              render json: {
                messages: "sub comment is deleted",
                is_success: true,
                data:{}
              }, status: :ok
            else
              render json: {
                messages: "cant delete sub comment, something is wrong",
                is_success: false,
                data:{}
              }, status: :conflict
            end
          else
            render json: {
              messages: "Unauthorized",
              is_success: false,
              data:{}
            }, status: :unauthorized
          end
        else
          render json: {
            messages: "comment-not-found",
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_subcomment
        @subcomment = Subcomment.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def subcomment_params
        params
          .require(:subcomment)
          .permit(:header, :content, :comment_id, :replytouser)
      end
  end
end