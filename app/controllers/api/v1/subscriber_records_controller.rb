
# describe 'a specification' do
module Api::V1 
  class SubscriberRecordsController < ApiController
    # GET /subscriber_records
    # GET /subscriber_records.json
    def index
      @subscriber_records = SubscriberRecord.all
    end

    # GET /subscriber_records/1
    # GET /subscriber_records/1.json
    def show

    end

    # POST /subscriber_records
    # POST /subscriber_records.json
    def create
      if current_user
        @subscriber_record = SubscriberRecord.new(subscriber_record_params)
        @subscriber_record.subscriber = current_user.id
        @subscriber_record.status = 'active'
        
        if @subscriber_record.save
          render json: @subscriber_record, status: :created
        else
          render json: @subscriber_record.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: 'Unauthenticated',
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    # PATCH/PUT /subscriber_records/1
    # PATCH/PUT /subscriber_records/1.json
    def update

      if(subscriber_record_params['status'] == 'no_action')
        render json: {
          messages: 'cant update to no_action, need to delete',
          is_success: false,
          data:{}
        }, status: 501
        return
      end

      if current_user
        @subscriber_record = SubscriberRecord.where(subscribeto: subscriber_record_params['subscribeto'], subscriber: current_user.id)[0]
        if @subscriber_record.present?
          if @subscriber_record.update(subscriber_record_params)
            render json: {
              messages: 'sub comment is updated',
              is_success: true,
              data: @subscriber_record
            }, status: :ok
          else
            render json: {
              messages: 'something is wrong',
              is_success: false,
              data:{}
            }, status: 500
          end
        else
          render json: {
            messages: 'subscription-not-found',
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: 'Unauthenticated',
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    # DELETE /subscriber_records/1
    # DELETE /subscriber_records/1.json
    def destroy
      if current_user
        @subscriber_record = SubscriberRecord.where(subscribeto: subscriber_record_params['subscribeto'], subscriber: current_user.id)[0]
        if @subscriber_record.present?
          if @subscriber_record.destroy
            render json: {
              messages: 'sub comment is deleted',
              is_success: true,
              data:{}
            }, status: :ok
          else
            render json: {
              messages: 'something is wrong',
              is_success: false,
              data:{}
            }, status: :error
          end
        else
          render json: {
            messages: 'subscription-not-found',
            is_success: false,
            data:{}
          }, status: :not_found 
        end
      else
        render json: {
          messages: 'Unauthenticated',
          is_success: false,
          data:{}
        }, status: :unauthorized
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_subscriber_record
        @subscriber_record = SubscriberRecord.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def subscriber_record_params
        params
          .require(:subscriber_record)
          .permit(:subscribeto, :status)
      end
  end
end