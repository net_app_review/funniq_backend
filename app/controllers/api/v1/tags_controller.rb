module Api::V1
  class TagsController < ApiController
    before_action :set_tag, only: [:show, :update, :destroy]
    # GET /tags
    # GET /tags.json
    def index
      @tags = Tag.all
      render json: @tags.to_json
    end

    def featured_tags
      # @featured_tag_list = Tag.includes(:articles).where.not( :articles=> { :id => nil } ).limit(20)
      # @featured_tag_list = Tag.all.order('created_at DESC').limit(20)
      @featured_tag_list = fetch_featured_tags_redis
      render json: @featured_tag_list
    end

    # GET /tags/1
    # GET /tags/1.json
    def show
      # @cate_articles = @tag.articles
      render json: @tag

    end

    # POST /tags
    # POST /tags.json
    def create
      @tag = Tag.new(category_params)

      if @tag.save
        render :show, status: :created, location: @tag
      else
        render json: @tag.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /tags/1
    # PATCH/PUT /tags/1.json
    def update
      if @tag.update(category_params)
        render :show, status: :ok, location: @tag
      else
        render json: @tag.errors, status: :unprocessable_entity
      end
    end

    # DELETE /tags/1
    # DELETE /tags/1.json
    def destroy
      @tag.destroy
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = Tag.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_params
      params.require(:tag).permit(:imageurl, :image, :icon)
    end

    def fetch_featured_tags_redis
      # @featured_tag_list = Tag.all.order('created_at DESC').limit(20)
      begin
        featured_tag_list = $redis.get "featured_tag_list"

        if featured_tag_list.nil?
          featured_tag_list = Tag.all.order("created_at DESC").limit(20).to_json
          $redis.set "featured_tag_list", featured_tag_list
        end

        featured_tag_list = JSON.load featured_tag_list
      rescue => error
        puts error.inspect
        featured_tag_list = Tag.all.order("created_at DESC").limit(20)
      end
      featured_tag_list
    end
  end
end