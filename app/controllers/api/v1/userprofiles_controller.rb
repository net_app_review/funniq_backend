module Api::V1
  class UserprofilesController < ApiController
    before_action :set_userprofile, only: [:destroy]

    # GET /userprofiles
    # GET /userprofiles.json
    def index
      @userprofiles = Userprofile.all
    end

    # GET /userprofiles/1
    # GET /userprofiles/1.json
    def current_user_profile
      if current_user then
        @userprofile = Userprofile.find_by(:user_id => current_user.id)
        render json: @userprofile
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end

    # POST /userprofiles
    # POST /userprofiles.json
    def create
      @userprofile = Userprofile.new(userprofile_params)

      if @userprofile.save
        render json: @userprofile, status: :ok
      else
        render json: @userprofile.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /userprofiles/1
    # PATCH/PUT /userprofiles/1.json
    def update
      if current_user then        
        @userprofile = current_user.userprofile
        if @userprofile.update(userprofile_params)
          render json: {
            messages: "updated user profile sucessfully",
            is_success: true,
            data: {
              userprofile: @userprofile
            }
          }, status: :ok
        else
          render json: @userprofile.errors, status: :unprocessable_entity
        end
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end    

    def update_image
      if current_user then     
        @userprofile = current_user.userprofile      
        if relationship_params[:image_url].present? && relationship_params[:image_height].present? && relationship_params[:image_width].present? then
          # if @userprofile.image.url.include? "https://ui-avatars.com/api/"
          #   if @userprofile.image.update!(url: relationship_params[:image_url]) then
          #     render json: {
          #       messages: "userprofile image url has been updated",
          #       is_success: true,
          #       data: {}
          #     }, status: :ok
          #   else
          #     render json: {
          #       messages: "cant update userprofile image url",
          #       is_success: true,
          #       data: {}
          #     }, status: :unprocessable_entity
          #   end
          # else
          if @userprofile.image.destroy then
            if Image.create!(
                url: relationship_params[:image_url],
                userprofile: @userprofile,
                height: relationship_params[:height],
                width: relationship_params[:width]
              ) then
                render json: {
                  messages: "userprofile image has been changed",
                  is_success: true,
                  data: {}
                }, status: :ok
            else
              render json: {
                messages: "cant change userprofile image",
                is_success: false,
                data: {}
              }, status: :unprocessable_entity
            end
          else
            render json: {
              messages: "cant delete current image",
              is_success: false,
              data: {}
            }, status: :unprocessable_entity
          end          
        end        
      else
        render json: {
          messages: "Unauthenticated",
          is_success: false,
          data:{}
        }, status: :unauthorized 
      end
    end


    # DELETE /userprofiles/1
    # DELETE /userprofiles/1.json
    def destroy
      @userprofile.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_userprofile
        @userprofile = Userprofile.find(params[:id])
      end

      def relationship_params
        params.require(:userprofile).permit(:image_url, :image_height, :image_width)
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def userprofile_params
        params.require(:userprofile).permit(:age, :gender, :birthday, :country, :description)
      end
  end
end