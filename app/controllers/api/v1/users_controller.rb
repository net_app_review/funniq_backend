require 'httparty'
require 'json'  
module Api::V1
  class UsersController < ApiController        
    include HTTParty
    include AuthenticateConcern
    # before_action :authenticate_with_token, only: [:index, :current, :update, :logout]
    before_action :set_user, only: [:show]
    def rankings
      ranked_users = User.where(:status => 'active').paginate(page: params[:page], per_page: 3).order(points: :desc)
      if ranked_users
          render json: { rankings: ActiveModelSerializers::SerializableResource.new(ranked_users, each_serializer: PublicUserSerializer), status: 200 }
          # render json: { rankings: ranked_users, status: 200 }
      else 
          render json: {status: 400 }
      end    
    end

    def weekly_rankings
      # ranked_users = User.where(:status => 'active').paginate(page: params[:page], per_page: 3).order(points: :desc)

      active_competition = Competition.where(status: 'active')[0]
      ranked_users = User.where(status: 'active')
                         .joins('INNER JOIN competitors ON users.id = competitors.user_id')
                         .where("competitors.competition_id = ?", active_competition.id)
                         .paginate(page: params[:page], per_page: 3)
                         .order('competitors.totalpoints DESC')

      if ranked_users
        render json: {  rankings: ActiveModelSerializers::SerializableResource.new(ranked_users, each_serializer: WeeklyrankSerializer),
                        competition: active_competition,
                        status: 200 }
      else
        render json: {status: 400 }
      end
    end

        def personal_ranking
            
        end

        def index
            render json: {status: 200, msg: 'logged-in'}
        end

        def show
            subscribed = 'no_action'
            if current_user
                if current_user.id != @user.id
                    record = SubscriberRecord.where(subscriber: current_user.id, subscribeto: @user.id)[0]
                    if record.present? 
                        subscribed= record.status
                    end
                else
                    subscribed = 'self'
                end
            end

            render json: {
                user: PublicUserSerializer.new(@user).attributes,
                subscribed: subscribed
            }
        end
        
        def current            
            if current_user then
                render json: {current_user: AuthenuserSerializer.new(current_user).attributes}
            else
                render json: {
                    messages: 'Unauthenticated',
                    is_success: false,
                    data:{}
                }, status: :no_content 
            end
        end

        def create
            user = User.new(user_params)
            
            if user.save
                
                render json: {status: 200, msg: 'User was created.'}
            end
        end

        def update_password
            if current_user then
                #If the password_init is true
                if !current_user.password_init
                    @user = current_user
                    @user.update(password_init: true)
                    if @user.update(user_params)
                        render json: {
                            message: 'password_init changed to true',
                            is_success: true, 
                            data: {                                 
                                user: AuthenuserSerializer.new(@user).attributes                                
                            }, status: :ok
                        }
                    else
                        render json: @user.errors, status: :unprocessable_entity
                    end
                else
                    if params[:old_password]
                        if (current_user.authenticate(params[:old_password]) != false)
                            @user = current_user                            
                            if @user.update(user_params)                        
                                render json: {
                                    message: 'password update ok',
                                    is_success: true, 
                                    data: {                                 
                                        user: AuthenuserSerializer.new(@user).attributes                                
                                    }, status: :ok
                                }
                            else
                                render json: @user.errors, status: :unprocessable_entity
                            end
                        else
                            render json: {
                                messages: 'Wrong old password',
                                is_success: false,
                                data:{}
                            },status: :not_acceptable
                        end
                    else
                        render json: {
                            messages: 'Missing old password',
                            is_success: false,
                            data:{}
                        },status: :not_found
                    end        
                end
            else
                render json: {
                    messages: 'Unauthenticated',
                    is_success: false,
                    data:{}
                }, status: :unauthorized 
            end
        end

    def check_password_changed_condition(user)
      can_change_password = false
      # threshold = (2*7*24*60*60)
      threshold = 1209600
      record = Counter.where(countingobj: "user_#{user.id}", countertype: 'user_password_changed').where('counters.count > ?', 2)[0]

      if record.present?
        pass_updated_at = record.updated_at
      end

      if !pass_updated_at.present?
        can_change_password = true
      else
        if (Time.now - pass_updated_at) > threshold
          can_change_password = true
        end
      end
      can_change_password
    end

        def update
          if current_user
            if !check_password_changed_condition(current_user)
              render json: {
                messages: 'Reached_limits',
                is_success: false,
                data: {}
              }, status: :unprocessable_entity
              return
            end

            @user = current_user
              if @user.update(user_params)
                if @user.saved_change_to_name.present?
                  record = Counter.where(countingobj: "user_#{current_user.id}", countertype: 'user_password_changed')[0]
                  if !record.present?
                    Counter.create!(
                      countingobj: "user_#{current_user.id}",
                      count: 1,
                      status: 'approved',
                      countertype: 'user_password_changed'
                    )
                  else
                    record.increment!(:count, 1)
                  end
                end
                render json: {
                  messages: 'updated user successfully',
                  is_success: true,
                  data: {
                    user: @user
                  }
                }, status: :ok
              else
                render json: @user.errors, status: :unprocessable_entity
              end
            else
              render json: {
                messages: 'Unauthenticated',
                is_success: false,
                data: {}
              }, status: :unauthorized
            end
        end

        def facebook_login
            if params[:facebook_access_token]
                begin
                    graph = Koala::Facebook::API.new params[:facebook_access_token]
                    
                    user_data = graph.get_object('me', fields:'email,id,picture,name')                

                    user = User.find_by email: user_data["email"]
                    if (user != nil && user.status == "active")                        
                        render json: { 
                            is_success: true, 
                            data: {                                 
                                user: AuthenuserSerializer.new(user).attributes                                
                            }, status: :ok
                        }

                    elsif (user != nil && user.status == "inactive")
                        render json: {
                            is_success: false,
                            messages: 'User is inactive',
                            user_status: user.status,
                            data: {}
                        }, status: :forbidden
                    else    
                        user = User.new(name: user_data["name"],
                                        email: user_data["email"],
                                        provider: "facebook",
                                        password: SecureRandom.alphanumeric(8),
                                        status: 'active',
                                        password_init: false)

                        user.authentication_token = User.generate_unique_secure_token

                        userprofile = Userprofile.new()
                        if user.save
                            userprofile.user = user
                            userprofile.save
                            avatar = Image.new( height: 50,
                                                width: 50,
                                                userprofile: userprofile,
                                                url: "https://ui-avatars.com/api/?name=#{user.slug}")
                            avatar.save
                            render json: {
                                messages: 'Sign up facebook sucessfully',
                                is_success: true,
                                data: {
                                    user: AuthenuserSerializer.new(user).attributes,
                                    # userprofile:userprofile,
                                    # avatar:avatar
                                }
                            }, status: :ok
                        else
                            render json: {
                                messages: 'Log in facebook not sucessfully',
                                is_success: false,
                                data: {}
                            }, status: :unprocessable_entity
                        end
                    end
                rescue Koala::Facebook::APIError => exception
                    if exception.fb_error_code == 190
                      render json: {is_success: false,
                                    error_code: '190'}
                    else
                      render json: {is_success: false,
                                    exception: exception}
                    end
                end
            else
                render json: {
                    messages: 'Missing facebook authenticity token',
                    is_success: false,
                    data: {}
                }, status: :unprocessable_entity
            end
        end

        def google
          if params[:id_token]
            # graph = Koala::Facebook::API.new params[:facebook_access_token]
            # user_data = graph.get_object('me', fields:'email,id,picture,name')
            url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{params["id_token"]}"
                
                response = HTTParty.get(url)

                user = User.find_by email: response['email']                

                if (user != nil && user.status == 'active')
                    # user.generate_new_authentication_token

                    user.save

                    render json: {
                        messages: 'Log in google sucessfully',
                        data:{
                            user: AuthenuserSerializer.new(user).attributes
                        }, status: :ok

                    }
                elsif (user != nil && user.status == 'inactive')
                    render json: {
                        messages: 'User is inactive',
                        user_status: user.status
                    }, status: :forbidden
                else



                    user = User.new(name: response["name"],
                                    email: response["email"],
                                    provider: "google",
                                    password: SecureRandom.alphanumeric(8),
                                    status: 'active',
                                    password_init: false)

                    user.authentication_token = User.generate_unique_secure_token
                    userprofile = Userprofile.new()

                    if user.save
                        userprofile.user = user
                        userprofile.save
                        avatar = Image.new(
                            height: 50,
                            width: 50,
                            userprofile: userprofile,
                            url: "https://ui-avatars.com/api/?name=#{user.slug}")
                        avatar.save
                        render json: {
                            messages: 'Sign up google sucessfully',
                            is_success: true,
                            data: {
                                user: AuthenuserSerializer.new(user).attributes
                            }
                        }, status: :ok
                    else
                        render json: {
                            messages: 'Sign up google not sucessfully',
                            is_success: false,
                            data: {}
                        }, status: :unprocessable_entity
                    end
                end
          else 
            render json: {
              messages: 'Missing google authenticity token',
              is_success: false,
              data: {}
            }, status: :unprocessable_entity
          end
        end

    private

    def set_user
      @user = User.find_by(uuid: params[:id].split('-')[0])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user_info).permit( :name, :password, :password_confirmation, :status, :level, :points )
    end
  end
end