module Api::V1
  class VoteCandidatesController < ApiController
    before_action :set_vote_candidate, only: [:show, :update, :destroy]
      
    # GET /vote_candidates
    # GET /vote_candidates.json
    def index
      @vote_candidates = VoteCandidate.all
      render :json => @vote_candidates.to_json
    end

    # GET /vote_candidates/1
    # GET /vote_candidates/1.json
    def show    
      # @cate_articles = @vote_candidate.articles
      render json: @vote_candidate

    end

    # POST /vote_candidates
    # POST /vote_candidates.json
    def create
      @vote_candidate = VoteCandidate.new(category_params)

      if @vote_candidate.save
        render :show, status: :created, location: @vote_candidate
      else
        render json: @vote_candidate.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /vote_candidates/1
    # PATCH/PUT /vote_candidates/1.json
    def update
      if @vote_candidate.update(category_params)
        render :show, status: :ok, location: @vote_candidate
      else
        render json: @vote_candidate.errors, status: :unprocessable_entity
      end
    end

    # DELETE /vote_candidates/1
    # DELETE /vote_candidates/1.json
    def destroy
      @vote_candidate.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_vote_candidate
        @vote_candidate = VoteCandidate.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def vote_candidate_params
        params.require(:vote_candidate).permit(:slogan, :color, :image_url, :profile, :name)
      end
  end
end