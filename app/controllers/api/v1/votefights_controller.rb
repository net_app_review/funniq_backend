module Api::V1
    class VotefightsController < ApiController
      before_action :set_votefight, only: [:show, :update, :destroy]
        
      # GET /votefights
      # GET /votefights.json
      def index
        # Article.where("header like ?", "%#{params[:search_string]}%").where(:status => "approved").present?
        # hot = Article.where(:status => "approved").paginate(page: params[:page], per_page: PAGINATION_NUMBER).order("view DESC")
        @votefights = Votefight.order("total_vote_count DESC")
        render :json => @votefights.to_json
      end
  
      # GET /votefights/1
      # GET /votefights/1.json
      def show    

        render json: @votefight
  
      end
  
      # POST /votefights
      # POST /votefights.json
      def create
        @votefight = Votefight.new(category_params)
  
        if @votefight.save
          render :show, status: :created, location: @votefight
        else
          render json: @votefight.errors, status: :unprocessable_entity
        end
      end
  
      # PATCH/PUT /votefights/1
      # PATCH/PUT /votefights/1.json
      def update
        if @votefight.update(category_params)
          render :show, status: :ok, location: @votefight
        else
          render json: @votefight.errors, status: :unprocessable_entity
        end
      end
  
      # DELETE /votefights/1
      # DELETE /votefights/1.json
      def destroy
        @votefight.destroy
      end
  
      private
        # Use callbacks to share common setup or constraints between actions.
        def set_votefight
          @votefight = Votefight.find(params[:id])
        end
  
        # Never trust parameters from the scary internet, only allow the white list through.
        def votefight_params
          params.require(:votefight).permit(:name, :end_date, :description, :slogan, :slug, :criteria, :start_date)
        end
    end
  end