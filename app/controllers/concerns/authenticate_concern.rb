module AuthenticateConcern
    def current_user
        auth_token = request.headers['X-User-Token']
        return unless auth_token
        @current_user = User.find_by authentication_token: auth_token
        if @current_user.present?
            # begin
            #     response = open('https://jsonip.com/').read
            #     data = JSON.parse(response)
            #     ip_address = data['ip']
            #   rescue
            #     ip_address = request.remote_ip
            # end
        
            
            

            if request.headers['X-Real-Ip'] != nil
                ip_address = request.headers['X-Real-Ip']
            else
                ip_address = request.remote_ip
            end

            @current_user.update!(lastloginip: ip_address)
        end
        return @current_user
    end

    def authenticate_with_token!
        return if current_user
        render json: {
            messages: 'Unauthenticated',
            is_success: false,
            data:{}
        }, status: :unauthorized
    end
end