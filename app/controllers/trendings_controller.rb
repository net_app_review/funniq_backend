class TrendingsController < ApplicationController
  before_action :set_trending, only: [:show, :update, :destroy]

  # GET /trendings
  # GET /trendings.json
  def index
    @trendings = Trending.all
  end

  # GET /trendings/1
  # GET /trendings/1.json
  def show
  end

  # POST /trendings
  # POST /trendings.json
  def create
    @trending = Trending.new(trending_params)

    if @trending.save
      render :show, status: :created, location: @trending
    else
      render json: @trending.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /trendings/1
  # PATCH/PUT /trendings/1.json
  def update
    if @trending.update(trending_params)
      render :show, status: :ok, location: @trending
    else
      render json: @trending.errors, status: :unprocessable_entity
    end
  end

  # DELETE /trendings/1
  # DELETE /trendings/1.json
  def destroy
    @trending.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trending
      @trending = Trending.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trending_params
      params.require(:trending).permit(:name, :description, :icon)
    end
end
