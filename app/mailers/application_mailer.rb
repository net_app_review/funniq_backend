class ApplicationMailer < ActionMailer::Base
  default from: 'funniqpage@gmail.com'
  layout 'mailer'
end
