class ReportedarticleMailer < ApplicationMailer
    def post_deactive(user, article)
        @user = user
        @article = article
        mail(to: @user.email, subject: 'Your post has been set inactive due to high number of reports')
    end
end