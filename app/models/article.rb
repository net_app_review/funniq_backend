class Article < ApplicationRecord
  after_validation :set_slug, only: [:create, :update]

  belongs_to :category
  belongs_to :user
  has_one :image, dependent: :destroy
  has_one :video, dependent: :destroy
  has_one :gif
  has_many :reports, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :article_tag_records
  has_many :tags, :through => :article_tag_records
  has_many :reports, dependent: :destroy
  has_many :notifications, dependent: :destroy
  after_create :increase_points
  after_create :increase_weekly_points
  after_create :increase_articles_count
  after_create :increase_user_articles_count
  validate :check_user_articles_limit
  after_destroy :decrease_points
  after_destroy :decrease_articles_count
  after_update :decrease_points
  after_update :decrease_articles_count
  before_create :set_uuid
  # before_save :validate_sensitivity
  # after_destroy :increase_points

  def to_param
    "#{id}-#{slug}"
  end

  POINTS_INCREASED = 20
  POINTS_DECREASED = 19
  ARTICLE_LIMIT = 75

  # def validate_sensitivity
  #   if sensitive
  #     self.category_id = Category.find_by_name('nsfw').id
  #     # if !self.tags.where(name: 'nsfw').present?
  #     #   ArticleTagRecord.create(article: self, tag: Tag.find_by_name('nsfw'))
  #     # end
  #   end
  # end

  def set_slug
    self.slug = header.to_s.parameterize
  end

  def increase_points
    if self.user
      self.user.increment(:points, POINTS_INCREASED)
      self.user.set_level
      self.user.save
    end
  end

  def increase_articles_count
    article_counter = Counter.find_by(countingobj: 'total_article')
    if !article_counter.present?
      article_counter = Counter.create!(
        countingobj: 'total_article',
        count: 0,
        status: 'approved',
        countertype: 'row_count'
      )
    end
    article_counter.increment!(:count, 1)
  end

  def decrease_articles_count
    if status == 'deleted'
      article_counter = Counter.find_by(countingobj: 'total_article')
      article_counter.decrement!(:count, 1)
    end
  end

  def increase_weekly_points
    active_competition = Competition.find_by_status('active')
    if user
      begin
        competitor = user.competitors.where(competition_id: active_competition.id)[0]
        if !competitor.present?
          competitor = Competitor.create!(competition: active_competition, user: self.user)
        end
        competitor.increment!(:totalpoints, POINTS_INCREASED)
      rescue => e
        print e
      end
    end
  end

  def set_uuid
    self.uuid = generate_uuid
  end
private

  def generate_uuid
    loop do
      # token = SecureRandom.hex(8)
      token = SecureRandom.alphanumeric(12)
      break token unless User.where(uuid: token).exists?
    end
  end

  def check_user_articles_limit
    condition_satisifed = false
    user_article_counter = Counter.find_by(countingobj: "user_#{user.id}")
    if !user_article_counter.present?
      condition_satisifed = true
    else
      if user_article_counter.count <= ARTICLE_LIMIT
        condition_satisifed = true
      end
    end

    unless condition_satisifed
      errors[:article_limit_attribute] << "you have reached this week's limit of #{ARTICLE_LIMIT}, your current article count is #{user_article_counter.count}"
      return false
    end
  end

  def increase_user_articles_count
    user_article_counter = Counter.find_by(countingobj: "user_#{user.id}")
    if !user_article_counter.present?
      user_article_counter = Counter.create!(
        countingobj: "user_#{user.id}",
        count: 0,
        status: 'approved',
        countertype: 'user_article_count'
      )
    end
    user_article_counter.increment!(:count, 1)
  end

  def decrease_points
    if status == 'deleted'
      active_competition = Competition.find_by_status('active')
      if user
        user.decrement(:points, POINTS_DECREASED)
        user.set_level
        user.save

        begin
          competitor = user.competitors.where(competition_id: active_competition.id)[0]
          if !competitor.present?
            competitor = Competitor.create!(competition: active_competition, user: self.user)
          end
          competitor.decrement!(:totalpoints, POINTS_DECREASED)
        rescue => e
          print e
        end

      end
    end
  end
end
