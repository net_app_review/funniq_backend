class ArticleTagRecord < ApplicationRecord
    belongs_to :tag
    belongs_to :article
    validates_uniqueness_of :article, :scope => [:tag]
end
  