class Category < ApplicationRecord
    after_validation :set_slug, only: [:create, :update]
    # belongs_to :user
    has_many :articles, dependent: :destroy
    has_one :image, dependent: :destroy
    accepts_nested_attributes_for :articles, allow_destroy: true
    validates_uniqueness_of :name
    attribute :article_count

    def to_param
        "#{id}-#{slug}"
    end

    def article_count( time=DateTime.now )
        self.articles.count
    end

private
    def set_slug
        self.slug = name.to_s.parameterize
    end 
end
