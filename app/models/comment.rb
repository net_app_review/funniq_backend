class Comment < ApplicationRecord
  belongs_to :article, optional: :true
  belongs_to :votefight, optional: :true
  belongs_to :user
  has_one :image, dependent: :destroy
  has_many :subcomments, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :likes, dependent: :destroy
  after_validation :increase_points, only: [:create]
  after_validation :increase_weekly_points, only: [:create]
  after_destroy :increase_points
  has_many :notifications, dependent: :destroy

  POINTS_INCREASED = 5

private
  def increase_points
    if self.user
      self.user.increment(:points, POINTS_INCREASED)
      self.user.set_level
      self.user.save
    end
  end  

  def increase_weekly_points
    active_competition = Competition.find_by_status('active')
    if user
      begin
        competitor = user.competitors.where(competition_id: active_competition.id)[0]
        if !competitor.present?
          competitor = Competitor.create!(competition: active_competition, user: self.user)
        end
        competitor.increment!(:totalpoints, POINTS_INCREASED)
      rescue => e
        print e
      end
    end
  end

  # def create_notification
  #   data = {}
  #   if article.present?
  #     data = {
  #       user_id: article.user_id,
  #       article_id: article.id,
  #       content: 'has commented on your post ',
  #       creator: user.id,
  #       iswatched: false
  #     }
  #   end
  #   Notification.create!(data)
  # end
  # def decrease_points
  #   if self.user
  #     self.user.decrement!(:points)
  #   end    
  # end  
end
