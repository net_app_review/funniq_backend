class Competition < ApplicationRecord
  has_many :users, through: :competitors
  has_many :competitors
end
