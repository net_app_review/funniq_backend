class Competitor < ApplicationRecord
  belongs_to :user
  belongs_to :competition
  
  def likes_count
    self.user.articles.sum(:like)
  end
end
