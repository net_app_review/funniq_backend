class Feedback < ApplicationRecord
  validates_presence_of :user_id
  validates_presence_of :email
  validates_presence_of :username
  validates_presence_of :content
  validates_presence_of :feedback_type
  has_many :likes, dependent: :destroy
end
