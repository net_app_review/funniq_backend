class FightCandiRec < ApplicationRecord
    belongs_to :votefight
    belongs_to :vote_candidate
    validates_uniqueness_of :votefight_id, :scope => [:vote_candidate_id]
end
  