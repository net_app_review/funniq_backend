class Image < ApplicationRecord
  # has_many :articles
  # belongs_to :user, optional: true
  belongs_to :userprofile, optional: true
  belongs_to :comment, optional: true
  # belongs_to :subcomment
  belongs_to :category, optional: true
  # belongs_to :tag
  belongs_to :article, optional: true
  belongs_to :trending, optional: true
  belongs_to :vote_candidate, optional: true

  # before_destroy :delete_firebase_image_first

  def delete_firebase_image_first

    @firebase = Firebase::Client.new(ENV["FIREBASE_DATABASE_URL"], ENV["FIREBASE_DATABASE_SECRET"])

    if self.url.include? "firebasestorage.googleapis.com"
      begin
        @firebase.delete(self.url)
      rescue StandardError => e 
        puts e.message
      end
      # if Application.find_by(join_test_job_id: "#{self.id}")
      #   self.errors.add(:base, "It's not possible to delete this item, because there are some applications related with. Please, delete them first.")
      #   throw(:abort) 
      # end
    end
  end
end
