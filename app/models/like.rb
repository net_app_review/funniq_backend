class Like < ApplicationRecord
  belongs_to :user
  belongs_to :article, optional: :true
  belongs_to :comment, optional: :true
  belongs_to :subcomment, optional: :true
  belongs_to :feedback, optional: :true
  validates_uniqueness_of :user_id, :scope => [:article_id, :comment_id, :subcomment_id, :feedback_id]
  validates :liketype, presence: true, inclusion: { in: %w(like dislike no_value upvote hide) }    
  after_validation :increase_points, only: [:create, :update]
  after_validation :increase_weekly_points, only: [:create, :update]

private

  POINTS_INCREASED = 2

  def increase_points
    if user
      user.increment(:points, POINTS_INCREASED)
      user.set_level
      user.save
    end
  end

  def increase_weekly_points
    active_competition = Competition.find_by_status('active')
    if user
      begin
        competitor = user.competitors.where(competition_id: active_competition.id)[0]
        if !competitor.present?
          competitor = Competitor.create!(competition: active_competition, user: self.user)
        end
        competitor.increment!(:totalpoints, POINTS_INCREASED)
      rescue => e
        print e
        # next # do_something_* again, with the next i
      end
    end
  end

  # def create_notification
  #   data = {}
  #   if article.present?
  #     data = {
  #       user_id: self.article.user_id,
  #       article_id: self.article.id,
  #       content: 'has liked your post ',
  #       creator: self.user.id,
  #       iswatched: false
  #     }
  #   elsif self.comment.present?
  #     data = {
  #       user_id: self.comment.user_id,
  #       comment_id: self.comment.id,
  #       article_id: self.comment.article.id,
  #       content: 'has liked your comment ',
  #       creator: self.user.id,
  #       iswatched: false
  #     }
  #   elsif self.subcomment.present?
  #     data = {
  #       user_id: subcomment.user_id,      
  #       subcomment_id: subcomment.id,
  #       article_id: self.subcomment.comment.article_id,
  #       content: 'has liked your comment in this post ',
  #       creator: self.user.id,
  #       iswatched: false
  #     }
  #   end

  #   Notification.create!(data)
  # end  
end
