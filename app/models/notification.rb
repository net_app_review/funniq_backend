class Notification < ApplicationRecord
  belongs_to :article, optional: :true
  belongs_to :user
  belongs_to :comment, optional: :true
  belongs_to :subcomment, optional: :true
  # after_validation :broadcast_now, only: [:create, :update]

private
  # def broadcast_now
  #   serialized_data = ActiveModelSerializers::Adapter::Json.new(
  #     NotificationSerializer.new(self)
  #   ).serializable_hash
  #   ActionCable.server.broadcast 'notification_channel', serialized_data    
  #   # ActionCable.server.broadcast 'notification_channel', { data: self, status: :ok}
  # end
end
