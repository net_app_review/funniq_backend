class Report < ApplicationRecord
  belongs_to :article, optional: :true
  belongs_to :user
  belongs_to :reportcategory
  belongs_to :comment, optional: :true
  belongs_to :subcomment, optional: :true
  validates_uniqueness_of :user_id, :scope => [:article_id, :comment_id, :subcomment_id, :reportcategory_id]
  # validates_presence_of :report_type
  after_validation :increase_points, only: [:create, :update]
  after_destroy :increase_points

private
  def increase_points                        
    if self.user
      self.user.increment(:points)
      self.user.set_level
      self.user.save
    end
  end

end
