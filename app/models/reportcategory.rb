class Reportcategory < ApplicationRecord
    has_many :reports
    validates_presence_of :name
    validates_uniqueness_of :name
end
