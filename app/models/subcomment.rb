class Subcomment < ApplicationRecord
  belongs_to :user
  belongs_to :comment
  has_many :reports, dependent: :destroy
  has_many :likes, dependent: :destroy
  after_validation :increase_points, only: [:create]
  after_validation :increase_weekly_points, only: [:create]
  # after_destroy :increase_points
  has_many :notifications, dependent: :destroy

  POINTS_INCREASED = 5

private
  def increase_points
    if self.user
       self.user.increment(:points)
       self.user.set_level
       self.user.save
    end
  end

  def increase_weekly_points
    active_competition = Competition.find_by_status('active')
    if user
      begin
        competitor = user.competitors.where(competition_id: active_competition.id)[0]
        if !competitor.present?
          competitor = Competitor.create!(competition: active_competition, user: self.user)
        end
        competitor.increment!(:totalpoints, POINTS_INCREASED)
      rescue => e
        print e
      end
    end
  end
end
