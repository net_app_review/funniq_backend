# describe
class SubscriberRecord < ApplicationRecord
  validates_uniqueness_of :subscriber, :scope => [:subscribeto]  
  validates :status, presence: true, inclusion: { in: %w(active inactive no_action) }
private
  # def create_notification
  #   data = {}

  #   noti = Notification.where( user_id: subscribeto,
  #                              creator: subscriber,
  #                              content: 'has subscribed to your content')

  #   if !noti.present?
  #     data = {
  #       user_id: subscribeto,
  #       content: 'has subscribed to your content',
  #       creator: subscriber,
  #       iswatched: false
  #     }
  #     Notification.create!(data)
  #   end
  # end
end
