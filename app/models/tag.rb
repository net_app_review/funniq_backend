class Tag < ApplicationRecord
    has_many :article_tag_records
    has_many :articles, :through => :article_tag_records
    validates_uniqueness_of :name
    attribute :article_count
    after_validation :set_slug, only: [:create, :update]

    def to_param
        "#{id}-#{slug}"
    end

    def article_count( time=DateTime.now )
        self.articles.count
    end

private
    def set_slug
        self.slug = name.to_s.parameterize
    end 
end
