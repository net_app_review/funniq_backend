class User < ApplicationRecord
  # acts_as_token_authenticatable
  after_validation :set_level, only: [:update]
  after_validation :set_slug, only: [:create, :update]
  after_create :generate_user_stats
  before_create :set_uuid

  validates :password, :presence => true,
                       :confirmation => true,
                       :length => {:within => 6..40},
                       :on => :create
  validates :password, :confirmation => true,
                       :length => {:within => 6..40},
                       :allow_blank => true,
                       :on => :update

  has_secure_password
  has_many :comments, dependent: :destroy
  has_many :subcomments, dependent: :destroy
  has_many :articles, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_one :userprofile, dependent: :destroy
  accepts_nested_attributes_for :articles, allow_destroy: true
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :name
  validates_uniqueness_of :uuid
  has_many :reports, dependent: :destroy
  has_many :competitors, dependent: :destroy
  has_many :notifications, dependent: :destroy

  has_many :competitions, through: :competitors

  def to_token_payload
    {
      sub: id,
      email: email
    }
  end

  def generate_new_authentication_token
    token = User.generate_unique_secure_token
    update_attributes authentication_token: token
  end

  def delete_authentication_token
    update_attributes authentication_token: ''
  end

  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.now.utc
    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
  end

  def to_param
    "#{id}-#{slug}"
  end

  def set_level
    case self.points
    when 0..10
      self.level = 1
    when 11..12
      self.level = 2
    when 13..14
      self.level = 4
    when 15..17
      self.level = 6
    when 18..19
      self.level = 7
    when 20..22
      self.level = 8
    when 23..25
      self.level = 9
    when 26..29
      self.level = 10
    when 30..33
      self.level = 11
    when 34..38
      self.level = 12
    when 39..45
      self.level = 13
    when 46..52
      self.level = 14
    when 53..62
      self.level = 15
    when 63..73
      self.level = 16
    when 74..87
      self.level = 17
    when 88..104
      self.level = 18
    when 105..125
      self.level = 19
    when 126..150
      self.level = 20
    when 151..180
      self.level = 21
    when 181..217
      self.level = 22
    when 218..260
      self.level = 23
    when 261..313
      self.level = 24
    when 314..376
      self.level = 25
    when 377..452
      self.level = 26
    when 453..543
      self.level = 27
    when 544..652
      self.level = 28
    when 653..783
      self.level = 29
    when 784..940
      self.level = 30
    when 941..1129
      self.level = 31
    when 1130..1355
      self.level = 32
    when 1356..1627
      self.level = 33
    when 1628..1953
      self.level = 34
    when 1954..2345
      self.level = 35
    when 2346..2814
      self.level = 36
    when 2815..3377
      self.level = 37
    when 3378..4053
      self.level = 38
    when 4054..4864
      self.level = 39
    when 4865..5836
      self.level = 40
    when 5837..7002
      self.level = 41
    when 7003..8401
      self.level = 42
    when 8402..10078
      self.level = 43
    when 10079..12088
      self.level = 44
    when 12089..14498
      self.level = 45
    when 14499..17388
      self.level = 46
    when 17389..20852
      self.level = 47
    when 20853..25003
      self.level = 48
    when 25004..29979
      self.level = 49
    when 29980..35942
      self.level = 50
    when 35943..43087
      self.level = 51
    when 43088..51648
      self.level = 52
    when 51649..61905
      self.level = 53
    when 61_906..74_194
      self.level = 54
    when 74_195..88_914
      self.level = 55
    else
      self.level = 56
    end
  end

  def set_uuid
    self.uuid = generate_uuid
  end

  def generate_uuid
    loop do
      # token = SecureRandom.hex(6)
      token = SecureRandom.alphanumeric(10)
      break token unless User.where(uuid: token).exists?
    end
  end

  private

  def set_slug
    self.slug = name.to_s.parameterize
  end

  def generate_token
    SecureRandom.hex(10)
  end

  def generate_user_stats
    self.level = 1
    self.points = 0
    save!
  end
end
