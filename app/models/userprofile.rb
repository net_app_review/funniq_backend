class Userprofile < ApplicationRecord
  belongs_to :user
  has_one :image, dependent: :destroy
  after_validation :create_age_over_13, only: [:create]

private
  def create_age_over_13
    self.age = 14
  end
end
