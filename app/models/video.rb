class Video < ApplicationRecord
  belongs_to :article
  # before_destroy :delete_firebase_video_first

  private
  def delete_firebase_video_first

    @firebase = Firebase::Client.new(ENV["FIREBASE_DATABASE_URL"], ENV["FIREBASE_DATABASE_SECRET"])        

    if self.url.include? "firebasestorage.googleapis.com"            
      begin
        @firebase.delete(self.url)
      rescue StandardError => e 
        puts e.message
      end
      # if Application.find_by(join_test_job_id: "#{self.id}")
      #   self.errors.add(:base, "It's not possible to delete this item, because there are some applications related with. Please, delete them first.")
      #   throw(:abort) 
      # end
    end
  end

end
