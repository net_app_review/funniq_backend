class VoteCandidate < ApplicationRecord    
    has_many :fight_candi_recs
    has_many :votefights, :through => :fight_candi_recs

    def vote_count
        return self.likes.where(liketype: "upvote").count
    end
end
