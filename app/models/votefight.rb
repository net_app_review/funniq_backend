class Votefight < ApplicationRecord
    after_validation :set_slug, only: [:create, :update]

    has_many :comments
    has_many :fight_candi_recs
    has_many :vote_candidates, :through => :fight_candi_recs
    
    
    # has_many :vote_criterias
    # has_many :likes
    # has_one :user

    def to_param
        "#{id}-#{slug}"
    end

    def total_vote_count
        candidate_list = self.vote_candidates
        total_vote = 0
        candidate_list.each do |c|
            total_vote = total_vote + c.likes.where(liketype: "upvote").count
        end
        return total_vote
    end

private
    def set_slug
        self.slug = name.to_s.parameterize
    end 
    
end
  