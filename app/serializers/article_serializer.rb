class ArticleSerializer < ActiveModel::Serializer
  attributes  :id, :uuid, :created_at,
              :elapsed_time, :header, :slug,
              :content, :video, :view,
              :comment_count, :user, :category,
              :image, :status, :gif, :like_count,
              :dislike_count, :like_status, :tags,
              :sensitive
  belongs_to :category
  belongs_to :user  
  belongs_to :image
  has_one :gif
  has_one :video
  has_many :tags
  # has_many :comments
  def comment_count
    comment_count = 0
    object.comments.each do |comment|
      comment_count = comment_count + comment.subcomments.count
    end
    comment_count = comment_count + object.comments.count
  end

  def like_count
    object.likes.where(liketype: "like").count
  end

  def dislike_count
    object.likes.where(liketype: "dislike").count
  end

  def like_status
    if current_user
      if object.likes.where(user_id: current_user.id)[0] then
        LikeSerializer.new(object.likes.where(user_id: current_user.id)[0], scope: scope, root: false, event: object)        
      else
        return "no_action"
      end
    else
      return "not_authenticated"
    end
  end

  def elapsed_time
    Time.now - (object.created_at || Time.now)
  end
end
