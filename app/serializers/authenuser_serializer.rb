class AuthenuserSerializer < ApplicationSerializer
  attributes :id, :name, :uuid,
             :slug, :email,
             :authentication_token,
             :status, :image, :password_init, :userprofile
  has_one :userprofile
  def image
    object.userprofile.image
  end
end
