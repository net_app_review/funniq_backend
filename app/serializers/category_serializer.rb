class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name,:slug, :icon, :appicon, :image, :description
  # has_many :articles
  has_one :image
end
