class CommentSerializer < ActiveModel::Serializer
  attributes  :id, :content, :user, :like_count,
              :dislike_count, :like_status, :image_url,
              :elapsed_time
  # belongs_to :user  
  # has_many :subcomments, serializer: SubcommentSerializer
  # def subcomments    
  #   object.subcomments.order("created_at DESC").map do |subcomment|
  #     SubcommentSerializer.new(subcomment, scope: scope, root: false, event: object)
  #   end
  # end

  def image_url
    if object.image
      return object.image.url 
    else
      return ""
    end
  end

  def like_count
    object.likes.where(liketype: "like").count
  end

  def dislike_count
    object.likes.where(liketype: "dislike").count
  end

  def user
    PublicUserSerializer.new(object.user, scope: scope, root: false, event: object)
  end

  def like_status
    curr_user = @instance_options[:current_user]
    if curr_user
      if object.likes.where(user_id: curr_user.id)[0] then        
        LikeSerializer.new(object.likes.where(user_id: curr_user.id)[0], scope: scope, root: false, event: object)        
      else
        return "no_action"
      end
    else
      return "not_authenticated"
    end
  end

  def elapsed_time
    Time.now - (object.created_at || Time.now)
  end
end
