class CompetitionSerializer < ApplicationSerializer
  attributes :id, :name, :description, :term, :status, :start, :end
end
