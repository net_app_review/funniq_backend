class CompetitorSerializer < ApplicationSerializer
  attributes :id, :paypalaccount, :totalpoints, :ranking
  has_one :user
  has_one :competition
end
