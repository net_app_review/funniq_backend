class FaqSerializer < ApplicationSerializer
  attributes :id, :question, :question_anchor, :answer
end
