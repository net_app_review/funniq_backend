class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :username, :email,
             :is_anonymous, :content,
             :rating, :feedback_type, :user_imageurl,
             :elapsed_time, :like_status, :like_count,
             :total_like_count

  def like_count
    object.likes.where(liketype: 'like').count
  end

  def total_like_count
    object.likes.count
  end

  def user_imageurl
    imageurl = nil

    authen_user = User.find_by_id(object.user_id)

    if authen_user.present?
      imageurl = authen_user.userprofile.image.url
    else
      imageurl = "https://ui-avatars.com/api/?name=#{object.email}"
    end
    imageurl
  end

  def username
    name = nil

    authen_user = User.find_by_id(object.user_id)

    if authen_user.present?
      name = authen_user.name
    else
      name = object.email
    end
    name
  end

  def like_status
    if current_user
      if object.likes.where(user_id: current_user.id)[0] then
        LikeSerializer.new(object.likes.where(user_id: current_user.id)[0], scope: scope, root: false, event: object)        
      else
        return "no_action"
      end
    else
      return "not_authenticated"
    end
  end

  def elapsed_time
    Time.now - (object.created_at || Time.now)
  end
end
