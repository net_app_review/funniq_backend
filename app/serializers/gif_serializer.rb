class GifSerializer < ApplicationSerializer
  attributes :id, :url, :height, :width
  # has_one :article
end
