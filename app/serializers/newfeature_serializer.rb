class NewfeatureSerializer < ApplicationSerializer
  attributes :id, :name, :platform, :description, :status, :start, :end
end
