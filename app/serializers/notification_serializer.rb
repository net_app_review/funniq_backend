class NotificationSerializer < ApplicationSerializer
  attributes  :id,
              :content,
              :created_user,
              :article, :comment_id,
              :subcomment_id, :iswatched

  def created_user
    if object.creator != nil
      return PublicUserSerializer.new(User.find(object.creator), scope: scope, root: false, event: object)    
    end
  end

  def article
    if object.article_id != nil
      return Article.joins("LEFT JOIN images ON articles.id = images.article_id" ).where(id: object.article_id).select(:id, :name, :url).take
    end
  end
end
