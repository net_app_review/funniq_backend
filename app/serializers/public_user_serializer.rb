class PublicUserSerializer < ApplicationSerializer
  attributes :id, :name, :uuid,
             :slug, :image,
             :country_code, :points, :post_view_count,
             :level, :rank, :subscribers_count,
             :subscribeto_count
  def image
    object.userprofile.image
  end
  
  def country_code
    if object.userprofile.country?
      ISO3166::Country.find_country_by_name(object.userprofile.country).alpha2
    else
      return ""
    end
  end

  def post_view_count
    count = 0
    if object.articles.present?
      object.articles.each do |article|
        if article.view != nil
          count = count + article.view
        end
      end
    end
    count
  end

  def vote_count
    object.likes.where(:liketype => "upvote").count
  end

  def rank
    # query = "SELECT GROUP_CONCAT (id, points)  FROM users "
    query = "SELECT id, name, points, FIND_IN_SET( points, (
        SELECT GROUP_CONCAT( points
        ORDER BY points DESC )
        FROM users )
        ) AS rank
        FROM users
        WHERE id =  #{object.id}"
    ranking_result = ActiveRecord::Base.connection.exec_query(query)
    personal_ranking = ranking_result[0]['rank']

    personal_ranking
  end

  def subscribers_count
    count = SubscriberRecord.where(subscribeto: object.id, status: 'active').count
    if count > 0
      return count
    else
      return 0
    end
  end

  def subscribeto_count
    count = SubscriberRecord.where(subscriber: object.id, status: 'active')
                            .count
    if count > 0
      return count
    else
      return 0
    end
  end

  # def subscribed
  #   subscribed = false

    


  #   if current_user
  #     subscribed = SubscriberRecord.where(subscriber: object.id,
  #                                         subscribeto: current_user.id,
  #                                         status: 'active').present?
  #   end

  #   subscribed

  # end
  # def comment_count
  #     return self.comments
  # end
end
