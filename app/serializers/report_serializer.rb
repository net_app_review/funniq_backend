class ReportSerializer < ApplicationSerializer
  attributes :id
  has_one :article
  has_one :user
end
