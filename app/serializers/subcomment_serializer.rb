class SubcommentSerializer < ActiveModel::Serializer
  attributes  :id, :content, :points, :user,
              :like_count, :dislike_count,
              :like_status, :replied_to, :elapsed_time

  def like_count
    object.likes.where(liketype: 'like').count
  end

  def dislike_count
    object.likes.where(liketype: 'dislike').count
  end

  def user
    PublicUserSerializer.new(object.user, scope: scope, root: false, event: object)
  end

  def like_status
    if current_user
      if object.likes.where(user_id: current_user.id)[0]
        LikeSerializer.new(object.likes.where(user_id: current_user.id)[0], scope: scope, root: false, event: object)        
      else
        'no_action'
      end
    else
      'not_authenticated'
    end
  end

  def replied_to
    User.where(id: object.replytouser).select(:id, :uuid, :name).take
  end

  def elapsed_time
    Time.now - (object.created_at || Time.now)
  end
end
