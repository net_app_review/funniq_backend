class TagSerializer < ActiveModel::Serializer
  attributes :id, :name,:imageurl, :icon, :articles_count, :slug 
  
  def articles_count    
    object.articles.count
  end
end
