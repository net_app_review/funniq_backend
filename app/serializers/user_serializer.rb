class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :uuid, :slug, :status, :email, :userprofile
  has_one :userprofile
  # def country_code
  #   ISO3166::Country.find_country_by_name(object.userprofile.country).alpha2
  # end
end
