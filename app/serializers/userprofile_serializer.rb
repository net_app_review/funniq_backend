class UserprofileSerializer < ApplicationSerializer
  attributes :id, :gender, :birthday, :country, :description, :image
  belongs_to :user
  has_one :image
end
