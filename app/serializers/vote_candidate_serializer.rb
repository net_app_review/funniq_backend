class VoteCandidateSerializer < ActiveModel::Serializer
    attributes :image_url, :profile, :slogan, :color, :vote_count, :name, :id, :vote_status

    def vote_status                  
      if current_user                  
        if object.likes.where(user_id: current_user.id)[0] then        
          LikeSerializer.new(object.likes.where(user_id: current_user.id)[0], scope: scope, root: false, event: object)        
        else
          return {liketype: "no_action"}
        end
      else
        return "not_authenticated"
      end
    end
end
  