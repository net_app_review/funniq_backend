class VotefightSerializer < ActiveModel::Serializer
    attributes :name, :criteria, :description, :slogan, :slug, :vote_candidates, :comment_count, :end_date_js

    has_many :vote_candidates, serializer: VoteCandidateSerializer do
        object.vote_candidates
              .joins("LEFT JOIN likes ON vote_candidates.id = likes.vote_candidate_id" )
              .where("likes.liketype = ?", "upvote")
              .group("vote_candidates.id")
              .order("COUNT(likes.id) DESC")
    end

    has_many :comments, serializer: CommentSerializer do
        object.comments.order("created_at DESC")
    end

    def comment_count
        comment_count = 0
      
        object.comments.each do |comment|
          comment_count = comment_count + comment.subcomments.count
        end
        comment_count = comment_count + object.comments.count
    end

    def end_date_js
        # Time.now() - end_date
        object.end_date.to_f * 1000
    end
end
  