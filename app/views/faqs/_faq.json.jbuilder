json.extract! faq, :id, :question, :question_anchor, :answer, :created_at, :updated_at
json.url faq_url(faq, format: :json)
