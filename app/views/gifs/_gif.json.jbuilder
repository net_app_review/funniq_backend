json.extract! gif, :id, :url, :height, :width, :article_id, :created_at, :updated_at
json.url gif_url(gif, format: :json)
