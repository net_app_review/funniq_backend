json.extract! newfeature, :id, :name, :platform, :description, :status, :start, :end, :created_at, :updated_at
json.url newfeature_url(newfeature, format: :json)
