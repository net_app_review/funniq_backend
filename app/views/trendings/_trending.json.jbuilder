json.extract! trending, :id, :name, :description, :icon, :created_at, :updated_at
json.url trending_url(trending, format: :json)
