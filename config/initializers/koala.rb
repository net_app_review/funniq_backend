# In Rails, you could put this in config/initializers/koala.rb
Koala.configure do |config|
    config.app_id = ENV["KOALA_GEM_APP_ID"]
    config.app_secret = ENV["KOALA_GEM_APP_ID_SECRET"]

    # KOALA_GEM_APP_ID: "1159821490885774"
    # KOALA_GEM_APP_ID_SECRET: "924a5e3329d34a8d8ac2c152e355d906"
    # See Koala::Configuration for more options, including details on how to send requests through
    # your own proxy servers.
  end