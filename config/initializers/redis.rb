# config/initializers/redis.rb
$redis = Redis::Namespace.new ENV["REDIS_DATABASE_NAMESPACE"], redis: Redis.new
