Rails.application.routes.draw do
     
  mount ActionCable.server => '/cable'
  resources :trendings
  resources :userprofiles
  resources :competitors
  resources :competitions
  devise_for :users
  resources :gifs
  resources :reportcategories
  
  resources :likes
  # resources :subcomments
  # resources :comments
  resources :videos  
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  delete :logout, to: "sessions#logout"
  get :logged_in, to: "sessions#logged_in"
  namespace :api do
    namespace :v1 do

      resources :feedbacks do
        collection do
          get 'index', to: 'feedbacks#index'
          post 'create', to: 'feedbacks#create'
          post 'update', to: 'feedbacks#update'
        end
      end

      resources :searchs do
        collection do
          get 'article', to: 'searchs#search_article'
          get 'tag', to: 'searchs#search_tag'
          get 'user', to: 'searchs#search_user'
        end
      end

      resources :subscriber_records do
        collection do
          post 'create', to: 'subscriber_records#create'
          post 'delete', to: 'subscriber_records#destroy'
          put 'update', to: 'subscriber_records#update'
        end
      end

      resources :notifications do
        collection do
          get 'index', to: 'notifications#index'
          post 'update', to: 'notifications#update'
        end
      end

      resources :newfeatures
      resources :faqs
      devise_scope :user do
        post "registrations", to: "registrations#create"
        post "sign_in", to: "sessions#create"
        delete "log_out", to: "sessions#destroy"
      end

      resources :votefights do
        collection do
         
        end
      end

      resources :password do
        collection do
          post 'forgot', to: 'password#forgot'
          post 'reset', to: 'password#reset'
        end
      end

      resources :likes do 
        collection do          
          # get "current_user", to: "user#current"          
          post "handle_like", to: "likes#handle_like_feature"
        end
      end
      #User routes      
      post "facebook", to: "users#facebook_login"
      post "google", to: "users#google"
      
      resources :users do 
        collection do          
          # get "current_user", to: "user#current"
          get "current_user", to: "users#current"
          post "update", to: "users#update"
          post "update_password", to: "users#update_password"
          get "rankings", to: "users#rankings"
          get "personal_ranking", to: "users#personal_ranking"
          get 'weekly_rankings', to: 'users#weekly_rankings'
        end
      end

      resources :userprofiles do 
        collection do                    
          get "current_user_profile", to: "userprofiles#current_user_profile"          
          post "update", to: "userprofiles#update"          
          post "update_image", to: "userprofiles#update_image"
        end
      end

      post 'user_token' => 'user_token#create'

      #images routes
      resources :images do
        collection do
          put 'upload_images' => 'images#request_for_presigned_url'
          post 'destroy' => 'images#destroy'
        end
      end
      resources :ideas      
      resources :articles do
        collection do
          get 'trending_articles' => 'articles#trending_articles'
          get 'fresh_articles' => 'articles#fresh_articles'
          get 'hot_articles' => 'articles#hot_articles'
          post 'create_article' => 'articles#create'
          post 'update_article' => 'articles#update'
          get 'category_articles' => 'articles#category_articles'
          get 'tag_articles' => 'articles#tag_articles'
          get 'search_article' => 'articles#search_article'
          get 'user_articles' => 'articles#user_articles'
          post 'destroy' => 'articles#destroy'
          post 'upload_to_facebook' => 'articles#upload_to_facebook'
        end
      end
      resources :comments do
        collection do
          get 'article_comments' => 'comments#article_comments'
          get 'votefight_comments' => 'comments#votefight_comments'
          post 'create' => 'comments#create'
          post 'destroy' => 'comments#destroy'
        end
      end
      resources :subcomments do
        collection do
          get 'comment_subcomments' => 'subcomments#comment_subcomments'
          post 'create' => 'subcomments#create'
          post 'destroy' => 'subcomments#destroy'
        end
      end

      resources :categories do
        collection do
          # get "category_articles" => 'categories#category_articles'
          # get "category_trend" => 'categories#category_trend'
        end
      end

      resources :tags do
        collection do
          get 'featured-tags' => 'tags#featured_tags'
          # get "category_articles" => 'categories#category_articles'
          # get "category_trend" => 'categories#category_trend'
        end
      end

      resources :reports do
        collection do
          post "create", to: "reports#create"
        end
      end

      resources :reportcategories do
        collection do
          
        end
      end
    end
  end  
end
