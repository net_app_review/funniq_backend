SitemapGenerator::Sitemap.default_host = "http://funniq.com/" # Your Domain Name
# SitemapGenerator::Sitemap.public_path = 'tmp/sitemap'
# Where you want your sitemap.xml.gz file to be uploaded.
# SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new( 
# aws_access_key_id: ENV["S3_ACCESS_KEY"],
# aws_secret_access_key: ENV["S3_SECRET_KEY"],
# fog_provider: 'AWS',
# fog_directory: ENV["S3_BUCKET_NAME"],
# fog_region: ENV["S3_REGION"]
# )

# The full path to your bucket
# SitemapGenerator::Sitemap.sitemaps_host = "https://#{'ENV["S3_BUCKET_NAME"]'}.s3.amazonaws.com"
# The paths that need to be included into the sitemap.
SitemapGenerator::Sitemap.create do
    Article.find_each do |article|
     add article_path(article.name, locale: :en)
     add article_path(article.slug, locale: :en)
    end

    add root_path
    # Project.find_each do |project|
    #  add project_path(project, locale: :en)
    #  add project_path(project, locale: :nl)
    # end
    # Page.find_each do |page|
    #  add page_path(page, locale: :en)
    #  add page_path(page, locale: :nl)
    # end

    # add "en/single-page"
    # add "nl/single-page"
    # add "nl/starters-website"
    # add "en/starters-website"
    # add "nl/website-op-maat"
    # add "en/website-op-maat"
    # add "nl/webapplicatie"
    # add "en/webapplicatie"
    # add "nl/website-analyse"
    # add "en/website-analyse"
end