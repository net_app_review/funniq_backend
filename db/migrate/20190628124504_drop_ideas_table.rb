class DropIdeasTable < ActiveRecord::Migration[5.1]
  def up
    drop_table :ideas
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
