class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :name
      t.string :link
      t.bigint :article, index: true, foreign_key: true
      t.bigint :user, index: true, foreign_key: true
      t.bigint :comment, index: true, foreign_key: true
      t.bigint :subcomment, index: true, foreign_key: true
      t.bigint :category, index: true, foreign_key: true
      t.bigint :tag, index: true, foreign_key: true

      t.timestamps
    end
  end
end
