class CreateTagAndTagRecords < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string :name
      t.string :image
      t.string :icon
      t.timestamps
    end
 
    create_table :article_tag_records do |t|
      t.belongs_to :article, index: true
      t.belongs_to :tag, index: true      
      t.timestamps
    end
  end
end
