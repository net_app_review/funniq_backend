class RemoveHeaderFromComment < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :header
  end
end
