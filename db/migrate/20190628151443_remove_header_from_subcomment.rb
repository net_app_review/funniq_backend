class RemoveHeaderFromSubcomment < ActiveRecord::Migration[5.1]
  def change
    remove_column :subcomments, :header
  end
end
