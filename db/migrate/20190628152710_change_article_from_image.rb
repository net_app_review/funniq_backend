class ChangeArticleFromImage < ActiveRecord::Migration[5.1]
  def change
    rename_column :images, :article, :article_id
  end
end
