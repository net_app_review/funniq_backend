class ChangeColumnsFromImage < ActiveRecord::Migration[5.1]
  def change
    rename_column :images, :user, :user_id
    rename_column :images, :comment, :comment_id
    rename_column :images, :subcomment, :subcomment_id
    rename_column :images, :category, :category_id
    rename_column :images, :tag, :tag_id
  end
end
