class RemoveColumnFromImage < ActiveRecord::Migration[5.1]
  def change
    remove_column :images, :article_id
    remove_column :images, :user_id
    remove_column :images, :comment_id
    remove_column :images, :subcomment_id
    remove_column :images, :category_id
    remove_column :images, :tag_id
  end
end
