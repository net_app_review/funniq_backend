class AddImageToArticle < ActiveRecord::Migration[5.1]
  def change
    add_reference :articles, :image, index: true, foreign_key: true
    add_reference :users, :image, index: true, foreign_key: true
    add_reference :comments, :image, index: true, foreign_key: true
    add_reference :subcomments, :image, index: true, foreign_key: true
    add_reference :categories, :image, index: true, foreign_key: true
    add_reference :tags, :image, index: true, foreign_key: true
  end
end
