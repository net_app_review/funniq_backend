class RemoveImageIdFromArticle < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :imageurl, :string
    add_column :comments, :imageurl, :string
    add_column :subcomments, :imageurl, :string
    add_column :tags, :imageurl, :string
    add_column :users, :imageurl, :string
  end
end
