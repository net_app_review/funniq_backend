class AddUserToArticle < ActiveRecord::Migration[5.1]
  def change
    add_reference :articles, :user, foreign_key: true, index: true
    add_reference :articles, :video, foreign_key: true, index: true
    add_reference :articles, :comment, foreign_key: true, index: true
  end
end
