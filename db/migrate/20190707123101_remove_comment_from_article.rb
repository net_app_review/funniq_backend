class RemoveCommentFromArticle < ActiveRecord::Migration[5.1]
  def change
    remove_reference :articles, :comment, foreign_key: true
  end
end
