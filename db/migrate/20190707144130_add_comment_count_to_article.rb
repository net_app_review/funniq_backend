class AddCommentCountToArticle < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :comment_count, :integer
  end
end
