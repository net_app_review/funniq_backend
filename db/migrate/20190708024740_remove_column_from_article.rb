class RemoveColumnFromArticle < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :comment_count
    remove_column :articles, :likenumber
  end
end
