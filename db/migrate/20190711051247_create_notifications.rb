class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.references :article, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
