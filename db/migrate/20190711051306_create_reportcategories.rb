class CreateReportcategories < ActiveRecord::Migration[5.1]
  def change
    create_table :reportcategories do |t|
      t.string :category

      t.timestamps
    end
  end
end
