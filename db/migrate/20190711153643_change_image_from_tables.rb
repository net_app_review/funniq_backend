class ChangeImageFromTables < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :image
    remove_column :categories, :image
    remove_column :categories, :imageurl
    remove_column :comments, :imageurl
    remove_column :subcomments, :imageurl
    remove_column :users, :imageurl
    remove_column :tags, :image
  end
end
