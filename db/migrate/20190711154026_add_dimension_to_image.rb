class AddDimensionToImage < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :height, :string
    add_column :images, :width, :string
  end
end
