class ModifyDimensionFromImages < ActiveRecord::Migration[5.1]
  def change
    change_column :images, :height, :decimal
    change_column :images, :width, :decimal
  end
end
