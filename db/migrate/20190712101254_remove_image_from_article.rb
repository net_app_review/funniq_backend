class RemoveImageFromArticle < ActiveRecord::Migration[5.1]
  def change
    remove_reference :articles, :image, index: true, foreign_key: true
  end
end
