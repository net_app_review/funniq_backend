class AddArticleReferenceToImage < ActiveRecord::Migration[5.1]
  def change
    add_reference :images, :article, index: true, foreign_key: true
  end
end
