class AddAppiconToCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :appicon, :string
  end
end
