class CreateGifs < ActiveRecord::Migration[5.1]
  def change
    create_table :gifs do |t|
      t.string :link
      t.integer :height
      t.integer :width
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
