class RemoveCommentidFromSubcomment < ActiveRecord::Migration[5.1]
  def change
    # remove_column :subcomments, :comment_id, :bigint
    remove_reference :subcomments, :comment, foreign_key: true
    # remove_foreign_key :subcomments, name: "index_subcomments_on_comment_id"
    # remove_reference :images, :article, foreign_key: true
  end
end
