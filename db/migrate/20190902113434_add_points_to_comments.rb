class AddPointsToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :points, :integer
    add_column :subcomments, :points, :integer
  end
end
