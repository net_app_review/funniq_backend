class RemoveArticleidFromSubcomment < ActiveRecord::Migration[5.1]
  def change
    remove_reference :subcomments, :article, foreign_key: true
  end
end
