class CreateCompetitions < ActiveRecord::Migration[5.1]
  def change
    create_table :competitions do |t|
      t.string :name
      t.text :description
      t.text :term
      t.string :status
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
