class CreateCompetitors < ActiveRecord::Migration[5.1]
  def change
    create_table :competitors do |t|
      t.references :user, foreign_key: true
      t.references :competition, foreign_key: true
      t.string :paypalaccount
      t.integer :totalpoints
      t.integer :ranking

      t.timestamps
    end
  end
end
