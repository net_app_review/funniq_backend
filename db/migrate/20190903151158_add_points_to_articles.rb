class AddPointsToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :points, :integer
  end
end
