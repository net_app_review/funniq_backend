class ChangeLinkFromImages < ActiveRecord::Migration[5.1]
  def change
    change_column :images, :link, :text
  end
end
