class AddDislikeToArticle < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :dislike, :integer
  end
end
