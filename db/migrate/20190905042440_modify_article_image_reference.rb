class ModifyArticleImageReference < ActiveRecord::Migration[5.1]
  def change
    remove_reference :users, :image, foreign_key: true
    add_reference :images, :user, index: true, foreign_key: true
  end
end
