class CreateUserprofiles < ActiveRecord::Migration[5.1]
  def change
    create_table :userprofiles do |t|
      t.references :user, foreign_key: true
      t.integer :age
      t.string :gender
      t.date :birthday
      t.string :country
      t.text :description

      t.timestamps
    end
  end
end
