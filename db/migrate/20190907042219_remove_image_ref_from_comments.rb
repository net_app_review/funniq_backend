class RemoveImageRefFromComments < ActiveRecord::Migration[5.1]
  def change
    remove_reference :comments, :image, foreign_key: true
    add_reference :images, :comment, index: true, foreign_key: true
  end
end
