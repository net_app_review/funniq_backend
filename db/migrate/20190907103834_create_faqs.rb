class CreateFaqs < ActiveRecord::Migration[5.1]
  def change
    create_table :faqs do |t|
      t.text :question
      t.text :question_anchor
      t.text :answer

      t.timestamps
    end
  end
end
