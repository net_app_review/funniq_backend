class FixLinkColumnFromImage < ActiveRecord::Migration[5.1]
  def change
    rename_column :images, :link, :url
  end
end
