class FixLinkColumnFromVideoandGif < ActiveRecord::Migration[5.1]
  def change
    change_column :gifs, :link, :text
    change_column :videos, :link, :text
    rename_column :gifs, :link, :url
    rename_column :videos, :link, :url
  end
end
