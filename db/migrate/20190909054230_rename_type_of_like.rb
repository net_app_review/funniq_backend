class RenameTypeOfLike < ActiveRecord::Migration[5.1]
  def change
    rename_column :likes, :type, :liketype
  end
end
