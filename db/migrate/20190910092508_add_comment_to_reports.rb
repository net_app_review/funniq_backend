class AddCommentToReports < ActiveRecord::Migration[5.1]
  def change
    remove_reference :reports, :reportcategory, foreign_key: true
    add_reference :reports, :comment, foreign_key: true
    add_reference :reports, :subcomment, foreign_key: true
    add_column :reports, :report_type, :string
    add_column :reports, :reason, :text
    add_column :reports, :severity, :integer
  end
end
