class RemoveImageRef < ActiveRecord::Migration[5.1]
  def change
    remove_reference :tags, :image, foreign_key: true
    remove_reference :subcomments, :image, foreign_key: true
  end
end
