class RemoveImageRelationshipFromCategory < ActiveRecord::Migration[5.1]
  def change
    remove_reference :categories, :image, foreign_key: true
    add_reference :images, :category, index: true, foreign_key: true
  end
end
