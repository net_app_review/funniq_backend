class CreateNewfeatures < ActiveRecord::Migration[5.1]
  def change
    create_table :newfeatures do |t|
      t.text :name
      t.string :platform
      t.text :description
      t.string :status
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
