class AddColumnsToVideo < ActiveRecord::Migration[5.1]
  def change
    add_column :videos, :height, :decimal
    add_column :videos, :width, :decimal
  end
end
