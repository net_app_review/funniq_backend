class CreateTrendings < ActiveRecord::Migration[5.1]
  def change
    create_table :trendings do |t|
      t.string :name
      t.text :description
      t.string :icon

      t.timestamps
    end
    add_reference :images, :trending, index: true, foreign_key: true
  end
end
