class AddRefToReport < ActiveRecord::Migration[5.1]
  def change
    add_reference :reports, :reportcategory, index: true, foreign_key: true
  end
end
