class RemovereportTypeFromReport < ActiveRecord::Migration[5.1]
  def change
    remove_column :reports, :report_type
    rename_column :reportcategories, :category, :name
    add_column :reportcategories, :description, :text
    add_column :reportcategories, :severity, :integer
  end
end
