class ChangeWidthFromImage < ActiveRecord::Migration[5.1]
  def change
    change_column :images, :width, :integer
    change_column :images, :height, :integer
  end
end
