class AddSlugToUsersAndCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :slug, :string
    add_column :categories, :slug, :string
  end
end
