class AddVoteCandidateToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :vote_candidate, :boolean
  end
end
