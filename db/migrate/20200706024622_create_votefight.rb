class CreateVotefight < ActiveRecord::Migration[5.1]
  def change
    create_table :votefights do |t|
      t.string :name
      t.datetime :date
      t.text :description
      t.string :slogan
      t.string :slug

      t.timestamps
    end
  end
end
