class CreateVoteCandidate < ActiveRecord::Migration[5.1]
  def change
    create_table :vote_candidates do |t|
      t.references :user, foreign_key: true
      t.text :slogan
    end
  end
end
