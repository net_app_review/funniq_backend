class CreateVoteCriteria < ActiveRecord::Migration[5.1]
  def change
    create_table :vote_criteria do |t|
      t.string :name
      t.text :description
    end
  end
end
