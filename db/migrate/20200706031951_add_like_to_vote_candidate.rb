class AddLikeToVoteCandidate < ActiveRecord::Migration[5.1]
  def change
    add_reference :likes, :vote_candidate, foreign_key: true
  end
end
