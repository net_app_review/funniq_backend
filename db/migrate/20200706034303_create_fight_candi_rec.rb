class CreateFightCandiRec < ActiveRecord::Migration[5.1]
  def change
    create_table :fight_candi_recs do |t|
      t.belongs_to :vote_candidate
      t.belongs_to :votefight      
    end
  end
end
