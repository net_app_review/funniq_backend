class AddVotefightToComment < ActiveRecord::Migration[5.1]
  def change
    add_reference :comments, :votefight, foreign_key: true
  end
end
