class AddVoteCandidatesAndFightToVoteCriteria < ActiveRecord::Migration[5.1]
  def change
    add_reference :vote_criteria, :vote_candidate, foreign_key: true
    add_reference :vote_criteria, :votefight, foreign_key: true
  end
end
