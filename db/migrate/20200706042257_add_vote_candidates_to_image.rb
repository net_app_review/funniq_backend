class AddVoteCandidatesToImage < ActiveRecord::Migration[5.1]
  def change
    add_reference :images, :vote_candidate, foreign_key: true
  end
end
