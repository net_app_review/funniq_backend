class AddColorToVoteCandidate < ActiveRecord::Migration[5.1]
  def change
    add_column :vote_candidates, :color, :string
  end
end
