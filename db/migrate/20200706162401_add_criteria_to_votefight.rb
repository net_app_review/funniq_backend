class AddCriteriaToVotefight < ActiveRecord::Migration[5.1]
  def change
    add_column :votefights, :criteria, :text
  end
end
