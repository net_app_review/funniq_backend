class RemoveUserFromVoteCandidate < ActiveRecord::Migration[5.1]
  def change
    remove_reference :vote_candidates, :user, foreign_key: true
  end
end
