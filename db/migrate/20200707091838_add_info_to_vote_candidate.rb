class AddInfoToVoteCandidate < ActiveRecord::Migration[5.1]
  def change
    add_column :vote_candidates, :image_url, :string
    add_column :vote_candidates, :profile, :text
    add_column :vote_candidates, :name, :string
  end
end
