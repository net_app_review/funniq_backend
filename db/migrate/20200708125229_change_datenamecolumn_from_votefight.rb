class ChangeDatenamecolumnFromVotefight < ActiveRecord::Migration[5.1]
  def change
    rename_column :votefights, :date, :end_date
    add_column :votefights, :start_date, :datetime
  end
end
