class AddReplytouserToSubcomment < ActiveRecord::Migration[5.1]
  def change
    add_column :subcomments, :replytouser, :integer
  end
end
