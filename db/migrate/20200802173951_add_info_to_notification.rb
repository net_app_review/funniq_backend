class AddInfoToNotification < ActiveRecord::Migration[5.1]
  def change
    add_reference :notifications, :comment, foreign_key: true
    add_reference :notifications, :subcomment, foreign_key: true
    add_column :notifications, :creator, :integer
    add_column :notifications, :iswatched, :boolean
  end
end
