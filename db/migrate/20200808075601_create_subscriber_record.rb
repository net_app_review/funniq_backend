class CreateSubscriberRecord < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriber_records do |t|
      t.integer :subscriber
      t.integer :subscribeto
      t.string  :status
    end
  end
end
