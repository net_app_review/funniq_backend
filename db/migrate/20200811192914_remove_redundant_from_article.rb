class RemoveRedundantFromArticle < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :sub_header
    remove_column :articles, :name
    remove_column :articles, :points
    remove_column :articles, :dislike
  end
end
