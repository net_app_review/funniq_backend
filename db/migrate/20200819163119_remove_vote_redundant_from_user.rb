class RemoveVoteRedundantFromUser < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :vote_candidate
    remove_column :users, :uid
  end
end
