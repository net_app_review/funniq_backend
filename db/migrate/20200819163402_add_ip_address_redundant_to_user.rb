class AddIpAddressRedundantToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :lastloginip, :string
  end
end
