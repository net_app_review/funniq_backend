class CreateCounter < ActiveRecord::Migration[5.1]
  def change
    create_table :counters do |t|
      t.string :countingobj
      t.bigint :count
      t.string :status
      t.string :type
    end
  end
end
