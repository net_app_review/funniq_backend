class ChangeTypeOfCounter < ActiveRecord::Migration[5.1]
  def change
    rename_column :counters, :type, :countertype
  end
end
