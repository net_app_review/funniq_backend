class CreateFeedback < ActiveRecord::Migration[5.1]
  def change
    create_table :feedbacks do |t|
      t.string :user_id
      t.string :email
      t.boolean :is_anonymous
      t.string :content
      t.integer :rating
      t.string :type
    end
  end
end
