class RemoveVoteCandidateFromLike < ActiveRecord::Migration[5.1]
  def change
    remove_reference :likes, :vote_candidate, foreign_key: true
  end
end
