class AddFeedbackToLike < ActiveRecord::Migration[5.1]
  def change
    add_reference :likes, :feedback, foreign_key: true
  end
end
