class UpdateTypeCandidateFromLike < ActiveRecord::Migration[5.1]
  def change
    rename_column :feedbacks, :type, :feedback_type
  end
end
