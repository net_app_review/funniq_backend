class AddUsernameToFeedback < ActiveRecord::Migration[5.1]
  def change
    add_column :feedbacks, :username, :string
  end
end
