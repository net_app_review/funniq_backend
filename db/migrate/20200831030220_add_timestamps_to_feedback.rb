class AddTimestampsToFeedback < ActiveRecord::Migration[5.1]
  def up
    add_timestamps :feedbacks, null: true
  end

  def down
    add_timestamps :feedbacks, null: true
  end
end
