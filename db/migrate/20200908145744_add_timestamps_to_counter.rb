class AddTimestampsToCounter < ActiveRecord::Migration[5.1]
  def up
    add_timestamps :counters, null: true
  end

  def down
    add_timestamps :counters, null: true
  end
end
