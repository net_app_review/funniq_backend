# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200908145744) do

  create_table "active_admin_comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "article_tag_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "article_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_article_tag_records_on_article_id"
    t.index ["tag_id"], name: "index_article_tag_records_on_tag_id"
  end

  create_table "articles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "header"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.integer "view"
    t.bigint "user_id"
    t.bigint "video_id"
    t.string "status"
    t.string "slug"
    t.boolean "sensitive"
    t.string "uuid"
    t.index ["category_id"], name: "index_articles_on_category_id"
    t.index ["user_id"], name: "index_articles_on_user_id"
    t.index ["video_id"], name: "index_articles_on_video_id"
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "appicon"
    t.text "description"
    t.string "slug"
  end

  create_table "comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "content"
    t.bigint "article_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "points"
    t.bigint "votefight_id"
    t.index ["article_id"], name: "index_comments_on_article_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
    t.index ["votefight_id"], name: "index_comments_on_votefight_id"
  end

  create_table "competitions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.text "description"
    t.text "term"
    t.string "status"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "competitors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "user_id"
    t.bigint "competition_id"
    t.string "paypalaccount"
    t.integer "totalpoints"
    t.integer "ranking"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["competition_id"], name: "index_competitors_on_competition_id"
    t.index ["user_id"], name: "index_competitors_on_user_id"
  end

  create_table "counters", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "countingobj"
    t.bigint "count"
    t.string "status"
    t.string "countertype"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "faqs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "question"
    t.text "question_anchor"
    t.text "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "user_id"
    t.string "email"
    t.boolean "is_anonymous"
    t.string "content"
    t.integer "rating"
    t.string "feedback_type"
    t.string "username"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fight_candi_recs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "vote_candidate_id"
    t.bigint "votefight_id"
    t.index ["vote_candidate_id"], name: "index_fight_candi_recs_on_vote_candidate_id"
    t.index ["votefight_id"], name: "index_fight_candi_recs_on_votefight_id"
  end

  create_table "gifs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "url"
    t.integer "height"
    t.integer "width"
    t.bigint "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_gifs_on_article_id"
  end

  create_table "images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.text "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "height"
    t.integer "width"
    t.bigint "article_id"
    t.bigint "user_id"
    t.bigint "userprofile_id"
    t.bigint "comment_id"
    t.bigint "category_id"
    t.bigint "trending_id"
    t.bigint "vote_candidate_id"
    t.index ["article_id"], name: "index_images_on_article_id"
    t.index ["category_id"], name: "index_images_on_category_id"
    t.index ["comment_id"], name: "index_images_on_comment_id"
    t.index ["trending_id"], name: "index_images_on_trending_id"
    t.index ["user_id"], name: "index_images_on_user_id"
    t.index ["userprofile_id"], name: "index_images_on_userprofile_id"
    t.index ["vote_candidate_id"], name: "index_images_on_vote_candidate_id"
  end

  create_table "likes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "article_id"
    t.bigint "user_id"
    t.bigint "comment_id"
    t.bigint "subcomment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "liketype"
    t.bigint "feedback_id"
    t.index ["article_id"], name: "index_likes_on_article_id"
    t.index ["comment_id"], name: "index_likes_on_comment_id"
    t.index ["feedback_id"], name: "index_likes_on_feedback_id"
    t.index ["subcomment_id"], name: "index_likes_on_subcomment_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "newfeatures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "name"
    t.string "platform"
    t.text "description"
    t.string "status"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "article_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "comment_id"
    t.bigint "subcomment_id"
    t.integer "creator"
    t.boolean "iswatched"
    t.index ["article_id"], name: "index_notifications_on_article_id"
    t.index ["comment_id"], name: "index_notifications_on_comment_id"
    t.index ["subcomment_id"], name: "index_notifications_on_subcomment_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "reportcategories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.integer "severity"
  end

  create_table "reports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "article_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "comment_id"
    t.bigint "subcomment_id"
    t.text "reason"
    t.integer "severity"
    t.bigint "reportcategory_id"
    t.index ["article_id"], name: "index_reports_on_article_id"
    t.index ["comment_id"], name: "index_reports_on_comment_id"
    t.index ["reportcategory_id"], name: "index_reports_on_reportcategory_id"
    t.index ["subcomment_id"], name: "index_reports_on_subcomment_id"
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "subcomments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "content"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "comment_id"
    t.integer "points"
    t.integer "replytouser"
    t.index ["comment_id"], name: "index_subcomments_on_comment_id"
    t.index ["user_id"], name: "index_subcomments_on_user_id"
  end

  create_table "subscriber_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "subscriber"
    t.integer "subscribeto"
    t.string "status"
  end

  create_table "tags", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "imageurl"
    t.string "slug"
  end

  create_table "trendings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.text "description"
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "userprofiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "user_id"
    t.integer "age"
    t.string "gender"
    t.date "birthday"
    t.string "country"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_userprofiles_on_user_id"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "provider"
    t.string "image"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "authentication_token", limit: 30
    t.string "status"
    t.boolean "password_init"
    t.string "slug"
    t.string "password_digest"
    t.bigint "points"
    t.integer "level"
    t.string "lastloginip"
    t.string "uuid"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "videos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.text "url"
    t.bigint "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "height", precision: 10
    t.decimal "width", precision: 10
    t.string "thumbnail"
    t.index ["article_id"], name: "index_videos_on_article_id"
  end

  create_table "vote_candidates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "slogan"
    t.string "color"
    t.string "image_url"
    t.text "profile"
    t.string "name"
  end

  create_table "vote_criteria", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.text "description"
    t.bigint "vote_candidate_id"
    t.bigint "votefight_id"
    t.index ["vote_candidate_id"], name: "index_vote_criteria_on_vote_candidate_id"
    t.index ["votefight_id"], name: "index_vote_criteria_on_votefight_id"
  end

  create_table "votefights", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.datetime "end_date"
    t.text "description"
    t.string "slogan"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "criteria"
    t.datetime "start_date"
  end

  add_foreign_key "articles", "users"
  add_foreign_key "articles", "videos"
  add_foreign_key "comments", "articles"
  add_foreign_key "comments", "users"
  add_foreign_key "comments", "votefights"
  add_foreign_key "competitors", "competitions"
  add_foreign_key "competitors", "users"
  add_foreign_key "gifs", "articles"
  add_foreign_key "images", "articles"
  add_foreign_key "images", "categories"
  add_foreign_key "images", "comments"
  add_foreign_key "images", "trendings"
  add_foreign_key "images", "userprofiles"
  add_foreign_key "images", "users"
  add_foreign_key "images", "vote_candidates"
  add_foreign_key "likes", "articles"
  add_foreign_key "likes", "comments"
  add_foreign_key "likes", "feedbacks"
  add_foreign_key "likes", "subcomments"
  add_foreign_key "likes", "users"
  add_foreign_key "notifications", "articles"
  add_foreign_key "notifications", "comments"
  add_foreign_key "notifications", "subcomments"
  add_foreign_key "notifications", "users"
  add_foreign_key "reports", "articles"
  add_foreign_key "reports", "comments"
  add_foreign_key "reports", "reportcategories"
  add_foreign_key "reports", "subcomments"
  add_foreign_key "reports", "users"
  add_foreign_key "subcomments", "comments"
  add_foreign_key "subcomments", "users"
  add_foreign_key "userprofiles", "users"
  add_foreign_key "videos", "articles"
  add_foreign_key "vote_criteria", "vote_candidates"
  add_foreign_key "vote_criteria", "votefights"
end
