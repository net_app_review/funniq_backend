require "#{Rails.root}/lib/tasks/helpers/task_helper"

namespace :batch do
  desc 'Send out batch messages'
  task send_messages: :environment do
    # The code to actually send our messages would go here
    puts 'runned tasks'
    puts Counter.first.status
  end

  desc 'Reset user articles limit counters'
  task reset_user_article_counter: :environment do
    # The code to actually send our messages would go here
    Counter.where(countertype: 'user_article_count').each do |c|
      c.update!(count: 0)
    end
  end

  desc 'Reset weekly rankings'
  task reset_weekly_rankings: :environment do
    today = Time.now()
    # Set up variables
    this_week_number = Time.now.strftime("%U").to_i
    next_week_number = this_week_number + 1
    next_start = today.next_week
    next_end = today.next_week + 7.day - 1
    next_competition_name = 'competition_' + next_week_number.to_s + '_' + today.year.to_s
    active_competitions = Competition.where(status: 'active')

    # Stop if next week is the same as current week

    # Check duplicate competition
    duplicate_comp = Competition.find_by(name: next_competition_name)

    # Create new competition
    if !duplicate_comp.present?

      # Disable old competitions
      active_competitions.each do |c|
        c.update!(status: 'inactive')
      end

      Competition.create!(
        name: 'competition_' + next_week_number.to_s + '_' + today.year.to_s,
        description: 'user ranks competitions',
        start: next_start,
        end: next_end,
        status: 'active'
      )
    else
      puts 'competition existed'
    end
  end
end
