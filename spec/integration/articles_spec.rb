require 'swagger_helper'

describe 'Article API' do

  path '/api/v1/articles/create_article' do

    post 'Creates an article' do
      tags 'Articles'
      consumes 'application/json', 'application/xml'
      parameter name: :header, in: :header, schema: {
        type: :object,
        properties: {
          "x-user-token": { type: :string },                            
        },
        required: [ 'x-user-token']
      }

      parameter name: :body, in: :body, schema: {
        type: :object,
        properties: {
          article: {
            type: :object,            
              properties: {
                name: { type: :string },                    
                header: { type: :string },
                sub_header: { type: :string },
                content: { type: :text },
                mediatype: { type: :string },
                category: { type: :string },
                url: { type: :string },
                status: "approved",
                tags: { type: :string },
                sensitive: { type: :string }
              }                            
            }            
        },
        required: [ 'header', 'category', 'mediatype', 'url', 'status']
      }

      response '201', 'article created' do
        let(:article) { { name: 'Dodo', status: 'available' } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:article) { { name: 'foo' } }
        run_test!
      end
    end
  end
end
