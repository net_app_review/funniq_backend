require "rails_helper"

RSpec.describe NewfeaturesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/newfeatures").to route_to("newfeatures#index")
    end

    it "routes to #show" do
      expect(:get => "/newfeatures/1").to route_to("newfeatures#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/newfeatures").to route_to("newfeatures#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/newfeatures/1").to route_to("newfeatures#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/newfeatures/1").to route_to("newfeatures#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/newfeatures/1").to route_to("newfeatures#destroy", :id => "1")
    end
  end
end
