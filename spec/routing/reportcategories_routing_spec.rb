require "rails_helper"

RSpec.describe ReportcategoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/reportcategories").to route_to("reportcategories#index")
    end

    it "routes to #show" do
      expect(:get => "/reportcategories/1").to route_to("reportcategories#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/reportcategories").to route_to("reportcategories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/reportcategories/1").to route_to("reportcategories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/reportcategories/1").to route_to("reportcategories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/reportcategories/1").to route_to("reportcategories#destroy", :id => "1")
    end
  end
end
