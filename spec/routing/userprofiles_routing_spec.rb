require "rails_helper"

RSpec.describe UserprofilesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/userprofiles").to route_to("userprofiles#index")
    end

    it "routes to #show" do
      expect(:get => "/userprofiles/1").to route_to("userprofiles#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/userprofiles").to route_to("userprofiles#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/userprofiles/1").to route_to("userprofiles#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/userprofiles/1").to route_to("userprofiles#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/userprofiles/1").to route_to("userprofiles#destroy", :id => "1")
    end
  end
end
